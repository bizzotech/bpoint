Meteor.startup(function () {
    ListView.publishAll();
    FormView.publishAll();
    SelectElement.publishAll();
    InnerList.publishAll();
});