FormView.registerPublication(Suppliers, [
    {
        name : '_id',
        collection : Purchases,
        relatedName : 'supplier',
    },
    {
        name : '_id',
        collection : SupplierPayments,
        relatedName : 'supplier',
    }
]);
SelectElement.registerPublication(Suppliers, [
    {
        name : '_id',
        collection : Purchases,
        relatedName : 'supplier',
    },
    {
        name : '_id',
        collection : SupplierPayments,
        relatedName : 'supplier',
    }
]);

ListView.registerPublication(Suppliers, [
    {
        name : '_id',
        collection : Purchases,
        relatedName : 'supplier',
    },
    {
        name : '_id',
        collection : SupplierPayments,
        relatedName : 'supplier',
    }
]);

FormView.registerPublication(Purchases, [
    {
        name : 'supplier',
        collection : Suppliers

    },
    {
        name : 'supplier',
        collection : Purchases,
        relatedName : 'supplier',
    },
    {
        name : 'supplier',
        collection : SupplierPayments,
        relatedName : 'supplier',
    }
]);
