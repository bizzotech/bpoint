FormView.registerPublication(SupplierPayments, [
    {
        name : 'supplier',
        collection : Suppliers

    },
    {
        name : 'supplier',
        collection : Purchases,
        relatedName : 'supplier',
    },
    {
        name : 'supplier',
        collection : SupplierPayments,
        relatedName : 'supplier',
    }
]);
SelectElement.registerPublication(SupplierPayments);
ListView.registerPublication(SupplierPayments, false, 'date');