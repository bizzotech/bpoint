FormView.registerPublication(Customers, [
    {
        name : '_id',
        collection : Sales,
        relatedName : 'customer',
    },
    {
        name : '_id',
        collection : CustomerPayments,
        relatedName : 'customer',
    }
]);
SelectElement.registerPublication(Customers, [
    {
        name : '_id',
        collection : Sales,
        relatedName : 'customer',
    },
    {
        name : '_id',
        collection : CustomerPayments,
        relatedName : 'customer',
    }
]);

ListView.registerPublication(Customers, [
    {
        name : '_id',
        collection : Sales,
        relatedName : 'customer',
    },
    {
        name : '_id',
        collection : CustomerPayments,
        relatedName : 'customer',
    }
]);

FormView.registerPublication(Sales, [
    {
        name : 'customer',
        collection : Customers

    },
    {
        name : 'customer',
        collection : Sales,
        relatedName : 'customer',
    },
    {
        name : 'customer',
        collection : CustomerPayments,
        relatedName : 'customer',
    }
]);
