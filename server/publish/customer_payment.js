FormView.registerPublication(CustomerPayments, [
    {
        name : 'customer',
        collection : Customers

    },
    {
        name : 'customer',
        collection : Sales,
        relatedName : 'customer',
    },
    {
        name : 'customer',
        collection : CustomerPayments,
        relatedName : 'customer',
    }
]);

SelectElement.registerPublication(CustomerPayments);
ListView.registerPublication(CustomerPayments, false, 'date');