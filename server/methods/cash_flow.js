Meteor.startup(function () {
    Meteor.methods({
        customerPaymentsValue : function(dateFrom, dateTo){
            var customerPayments = CustomerPayments.find({
                valid : true, 
                date : { $gte : new Date(dateFrom) , $lte : new Date(dateTo) }
            }).fetch();
            var total = 0;
            _.each(customerPayments, function(payment){
                total += payment.value;
            });

            //Meteor._sleepForMs(2000);

            return Math.round(total);
        },
        supplierPaymentsValue : function(dateFrom, dateTo){
            var supplierPayments = SupplierPayments.find({
                valid : true, 
                date : { $gte : new Date(dateFrom) , $lte : new Date(dateTo) }
            }).fetch();
            var total = 0;
            _.each(supplierPayments, function(payment){
                total += payment.value;
            });

            //Meteor._sleepForMs(5000);

            return Math.round(total);
        },
        expensesValue : function(dateFrom, dateTo){
            var expenses = Expenses.find({
                valid : true, 
                date : { $gte : new Date(dateFrom) , $lte : new Date(dateTo) }
            }).fetch();
            var total = 0;
            _.each(expenses, function(expense){
                total += expense.value;
            });

            //Meteor._sleepForMs(3000);

            return Math.round(total);
        }
    });
});