Meteor.startup(function () {
    Meteor.methods({
        customerDues : function(){
            var customers = Customers.find().fetch();
            var total = 0;
            _.each(customers, function(customer){
                total += customer.toBePaid();
            });
            return Math.round(total);
        },
        supplierDues : function(){
            var suppliers = Suppliers.find().fetch();
            var total = 0;
            _.each(suppliers, function(supplier){
                total += supplier.toBePaid();
            });
            return Math.round(total);
        }
    });
});