Meteor.startup(function () {
    Meteor.methods({
        salesValue : function(dateFrom, dateTo){
            var sales = Sales.find({
                valid : true, 
                date : { $gte : new Date(dateFrom) , $lte : new Date(dateTo) }
            }).fetch();
            var total = 0;
            _.each(sales, function(sale){
                total += sale.total;
            });

            //Meteor._sleepForMs(2000);

            return Math.round(total);
        },
        saleReturnsValue : function(dateFrom, dateTo){
            var saleReturns = SaleReturns.find({
                valid : true, 
                date : { $gte : new Date(dateFrom) , $lte : new Date(dateTo) }
            }).fetch();
            var total = 0;
            _.each(saleReturns, function(saleReturn){
                total += saleReturn.total;
            });

            //Meteor._sleepForMs(2000);

            return Math.round(total);
        },
        cogsValue : function(dateFrom, dateTo){
            var sales = Sales.find({
                valid : true, 
                date : { $gte : new Date(dateFrom) , $lte : new Date(dateTo) }
            }).fetch();
            var total = 0;
            _.each(sales, function(sale){
                total += sale.totalCost();
            });

            var saleReturns = SaleReturns.find({
                valid : true, 
                date : { $gte : new Date(dateFrom) , $lte : new Date(dateTo) }
            }).fetch();
            _.each(saleReturns, function(saleReturn){
                total -= saleReturn.totalCost();
            });

            //Meteor._sleepForMs(5000);

            return Math.round(total);
        }/*,
        expensesValue : function(dateFrom, dateTo){
            var expenses = Expenses.find({
                valid : true, 
                date : { $gte : new Date(dateFrom) , $lte : new Date(dateTo) }
            }).fetch();
            var total = 0;
            _.each(expenses, function(expense){
                total += expense.value;
            });

            Meteor._sleepForMs(3000);

            return total;
        }*/
    });
});