Meteor.startup(function () {
    Meteor.methods({
        stockValue : function(){
            var proucts = Products.find().fetch();
            var total = 0;
            _.each(proucts, function(prouct){
                total += prouct.stockValue();
            });
            return Math.round(total);
        }
    });
});