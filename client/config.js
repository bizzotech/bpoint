var requireLogin = function() {  
	if (! Meteor.user()) {
	    this.render('accessDenied');  
	} else {
	    this.next();  
	}
}

Router.configure({
    loadingTemplate : 'Loading',
    layoutTemplate: 'ApplicationLayout'
});

Router.onBeforeAction(requireLogin);

Accounts.ui.config({
  passwordSignupFields: 'USERNAME_ONLY'
});

accountsUIBootstrap3.setLanguage('ar');