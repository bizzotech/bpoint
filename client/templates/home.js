Router.route('/', {
  action : function () {
    if (! Meteor.user().profile.limited) {
        this.render('home');
    }else {
        this.next();
    }
    
  }
});