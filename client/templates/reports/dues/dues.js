Template.dues.created = function(){
    var instance = this;

    this.customerDues = new ReactiveVar('جاري الحساب');
    this.supplierDues = new ReactiveVar('جاري الحساب');
    
    Meteor.call('customerDues', function (error, result) {
        instance.customerDues.set(result);
    });
    Meteor.call('supplierDues', function (error, result) {
        instance.supplierDues.set(result);
    });
}

Template.dues.helpers({
    'isLoading' : function(){
        var instance = Template.instance();
        var customerDues = Number(instance.customerDues.get());
        var supplierDues = Number(instance.supplierDues.get());
        return isNaN(customerDues) || isNaN(supplierDues);
    },
    customerDues : function(){
        var instance = Template.instance();
        return instance.customerDues.get();
    },
    supplierDues : function(){
        var instance = Template.instance();
        return instance.supplierDues.get();
    }    
});