Template.profitAndLoss.created = function () {

    var instance = this;
  
    var dateTo = moment().format('YYYY-MM-DD');
    var dateFrom = moment().subtract(7,'d').format('YYYY-MM-DD');
    
    this.from = new ReactiveVar(dateFrom);
    this.to = new ReactiveVar(dateTo);
    this.salesValue = new ReactiveVar('جاري الحساب');
    this.saleReturnsValue = new ReactiveVar('جاري الحساب');
    this.cogsValue = new ReactiveVar('جاري الحساب');
    this.expensesValue = new ReactiveVar('جاري الحساب');
    
    instance.autorun(function(){
        var from = instance.from.get();
        var to = instance.to.get();
        var dateTo = moment(new Date(to)).add(1,'d').format('YYYY-MM-DD');
        
        instance.salesValue.set('جاري الحساب');
        instance.saleReturnsValue.set('جاري الحساب');
        instance.cogsValue.set('جاري الحساب');
        instance.expensesValue.set('جاري الحساب');
        
        Meteor.call('salesValue', from, dateTo, function (error, result) {
            instance.salesValue.set(result);
        });
        Meteor.call('saleReturnsValue', from, dateTo, function (error, result) {
            instance.saleReturnsValue.set(result);
        });
        Meteor.call('cogsValue', from, dateTo, function (error, result) {
            instance.cogsValue.set(result);
        });
        Meteor.call('expensesValue', from, dateTo, function (error, result) {
            instance.expensesValue.set(result);
        });
    });

};


Template.profitAndLoss.helpers({
    'isLoading' : function(){
        var instance = Template.instance();
        var salesValue = Number(instance.salesValue.get());
        var saleReturnsValue = Number(instance.saleReturnsValue.get());
        var cogsValue = Number(instance.cogsValue.get());
        var expensesValue = Number(instance.expensesValue.get());
        return isNaN(salesValue) || isNaN(saleReturnsValue) || isNaN(cogsValue) || isNaN(expensesValue);
    },
    dateFrom : function(){
      return Template.instance().from.get();
    },
    dateTo : function(){
      return Template.instance().to.get();
    },
    salesValue : function(){
      var instance = Template.instance();
      return instance.salesValue.get();
    },
    saleReturnsValue : function(){
      var instance = Template.instance();
      return instance.saleReturnsValue.get();
    },
    cogsValue : function(){
      var instance = Template.instance();
      return instance.cogsValue.get();
    },
    grossProfitValue : function(){
      var instance = Template.instance();
        var salesValue = Number(instance.salesValue.get());
        var saleReturnsValue = Number(instance.saleReturnsValue.get());
        var cogsValue = Number(instance.cogsValue.get());
        if(isNaN(Number(cogsValue)) || isNaN(Number(salesValue)) || isNaN(Number(saleReturnsValue))){
          return "جاري الحساب";
        }else {
          return salesValue - saleReturnsValue - cogsValue;
        }
    },
    expensesValue : function(){
      var instance = Template.instance();
      return instance.expensesValue.get();
    },
    netProfitValue : function(){
      var instance = Template.instance();
      var salesValue = Number(instance.salesValue.get());
      var saleReturnsValue = Number(instance.saleReturnsValue.get());
      var cogsValue = Number(instance.cogsValue.get());
      var expensesValue = Number(instance.expensesValue.get());
      if (isNaN(salesValue) || isNaN(Number(saleReturnsValue)) || isNaN(cogsValue) || isNaN(expensesValue) ){
        return "جاري الحساب"; 
      }

      return salesValue - saleReturnsValue - cogsValue - expensesValue; 
    }
  
});

Template.profitAndLoss.events({
  "input #from" : function(event, template){
    var value = $(event.target).val();
    template.from.set(value);
  },
  "input #to" : function(event, template){
    var value = $(event.target).val();
    template.to.set(value);
  },
  
});