Template.stock.created = function(){
    var instance = this;

    this.stockValue = new ReactiveVar('جاري الحساب');
    
    Meteor.call('stockValue', function (error, result) {
        instance.stockValue.set(result);
    });
    
}

Template.stock.helpers({
    'isLoading' : function(){
        var instance = Template.instance();
        var stockValue = Number(instance.stockValue.get());
        return isNaN(stockValue)
    },
    stockValue : function(){
        var instance = Template.instance();
        return instance.stockValue.get();
    }
});