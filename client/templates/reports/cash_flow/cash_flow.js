Template.cashFlow.created = function () {

    var instance = this;
  
    var dateTo = moment().format('YYYY-MM-DD');
    var dateFrom = moment().format('YYYY-MM-DD');
    
    this.from = new ReactiveVar(dateFrom);
    this.to = new ReactiveVar(dateTo);
    
    this.customerPaymentsValue = new ReactiveVar('جاري الحساب');
    this.supplierPaymentsValue = new ReactiveVar('جاري الحساب');
    this.expensesValue = new ReactiveVar('جاري الحساب');

    instance.autorun(function(){
        var from = instance.from.get();
        var to = instance.to.get();
        var dateTo = moment(new Date(to)).add(1,'d').format('YYYY-MM-DD');
        
        instance.customerPaymentsValue.set('جاري الحساب');
        instance.supplierPaymentsValue.set('جاري الحساب');
        instance.expensesValue.set('جاري الحساب');
        
        Meteor.call('customerPaymentsValue', from, dateTo, function (error, result) {
            instance.customerPaymentsValue.set(result);
        });
        Meteor.call('supplierPaymentsValue', from, dateTo, function (error, result) {
            instance.supplierPaymentsValue.set(result);
        });
        Meteor.call('expensesValue', from, dateTo, function (error, result) {
            instance.expensesValue.set(result);
        });
    });
    
    
};


Template.cashFlow.helpers({
    'isLoading' : function(){
        var instance = Template.instance();
        var customerPaymentsValue = Number(instance.customerPaymentsValue.get());
        var supplierPaymentsValue = Number(instance.supplierPaymentsValue.get());
        var expensesValue = Number(instance.expensesValue.get());
        return isNaN(customerPaymentsValue) || isNaN(supplierPaymentsValue) || isNaN(expensesValue);
    },
    dateFrom : function(){
      return Template.instance().from.get();
    },
    dateTo : function(){
      return Template.instance().to.get();
    },
    customerPaymentsValue : function(){
      var instance = Template.instance();
      return instance.customerPaymentsValue.get();
    },
    supplierPaymentsValue : function(){
      var instance = Template.instance();
      return instance.supplierPaymentsValue.get();
    },
    expensesValue : function(){
      var instance = Template.instance();
      return instance.expensesValue.get();
    },
    netCashFlow : function(){
      return Template.instance().customerPaymentsValue.get() - Template.instance().supplierPaymentsValue.get() - Template.instance().expensesValue.get(); 
    }
  
});

Template.cashFlow.events({
  "input #from" : function(event, template){
    var value = $(event.target).val();
    template.from.set(value);
  },
  "input #to" : function(event, template){
    var value = $(event.target).val();
    template.to.set(value);
  },
  
});