Template.Header.helpers({
    active: function(linkName) {
        var routeName = Router.current().route.getName();
        return (routeName === linkName) ? ' active ' : '';
      }
});