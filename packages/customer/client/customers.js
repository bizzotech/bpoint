var customerList = {
  name : "customer",
  title : "قائمة العملاء",
  fields : [
    {
      name : "name",
      label : "الاسم",
    },
    {
      name : "phoneNumber",
      label : "رقم التيليفون",
    },
    {
      name : "address",
      label : "العنوان",
    }
  ],
  tableClass : function(){
    return "table table-stripped table-hover";
  },
  fieldClass : function(object, fieldName){
    return fieldName;
  },
  virtualColumns : [
    {
      name : "payMethod",
      label : " طريقة الدفع ",
      fn : function(customer){
         if(customer.payMethod == "1"){
            return "ﺑﻄﺎﻗﺔ اﺋﺘﻤﺎﻥ" ;
          }
          if(customer.payMethod == "2"){
            return "ﺗﺤﻮﻳﻞ ﺭاﺗﺐ";
          }
          if(customer.payMethod == "3"){
            return "ﻧﻘﺪﻱ";
          }
          return "ﻏﻴﺮ ﻣﻌﺮﻭﻑ";
      }
    }, 
    {
      name : "toBePaid",
      label : "المتبقي عليه",
      fn : function(customer){
        var val = customer.toBePaid();
        return Number(Number(val).toFixed(2)) || 0; 
      },
    },
    
  ]
}

ListView.registerList(Customers, customerList);
