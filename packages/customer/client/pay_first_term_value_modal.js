Template.payCFirstTermValueModal.events({
  'click #payButton' : function(event, template){
     event.preventDefault();
     var paymentValue = Number($('#paymentValue').val());
     var customer = template.data.object;
     Modal.hide('payCFirstTermValueModal');
      
     var toPay = customer.remainigOfFirstTermValue();
      if(paymentValue > toPay){
          throwError(" لا يمكن دفع مبلغ اكبر من المتبقي " + toPay);
          return 
      }
      Meteor.call('payCFirstTermValue', customer._id, paymentValue);
  }
});