var customerPaymentForm = {
    collection : CustomerPayments,
    name : "customerpayment",
    class : "",
    createTitle : " تسجيل مدفوعات " ,
    readTitle : " بيانات مدفوعات ",
    updateTitle : " تعديل بيانات مدفوعات " ,
    fieldGroups : [
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "العميل",
                    name : "customer",
                    type : "object",
                    collection : "customers",
                    formName : "customer",
                    required : true,
                },
                {
                    label : "اسم العميل",
                    name : "customerName",
                    type : "text",
                    fn : function(customer){
                        if(customer != undefined && customer != ""){
                            return Customers.findOne(customer).name; 
                        }
                        return "";
                    },
                    dependOn : ["customer"],
                    style : "display : none",
                },

                {
                    class : "col-xs-6",
                    label : "التاريخ",
                    name : "date",
                    type : "date",
                    defaultValue : function(){
                        return  new Date();
                    }
                }
            ]
        },

        {
            class : "",
            fields : [
                {
                    class : "col-xs-12",
                    label : "تم التأكيد",
                    name : "valid",
                    type : "state",
                    visible : function(){
                        return false;
                    },
                }
            ]
        },
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : " المبلغ ",
                    name : "value",
                    type : "number",
                    required : true,
                },
                {
                    class : "col-xs-6",
                    label : " المتبقي عليه ",
                    type : "number",
                    fn : function(customer){
                        if(customer != undefined && customer != "" ){
                            return  Customers.findOne(customer).toBePaid();
                        }
                        return 0;
                    },
                    dependOn : ['customer'],
                    readOnly : true,
                    virtual : true,
                }
            ]
        }
    ],
    submitButtonContent : "حفظ",
    editButtonContent : "تعديل",
    deleteButtonContent : "حذف",
    isEditable : function(collection, object, fields){
        return object.valid != true && object.valid != "true";
    },
    isDeletable : function(collection, object, fields){
        return object.valid != true && object.valid != "true";
    },
    customButtons : [
        {
            name : "confirm",
            label : "تأكيد",
            fn : function(collection, object, fields){
                if(object != undefined && object._id != undefined){
                    Meteor.call('validateCustomerPayment', object);
                }
            },
            visible : function(collection, object, fields, formType){
                return formType == "read" && object.valid != true;
            }
        }
    ]
}

FormView.registerForm(CustomerPayments, customerPaymentForm);