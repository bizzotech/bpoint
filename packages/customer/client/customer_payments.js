var customerPaymentList = {
  name : "customerpayment",
  title : "قائمة متحصلات العملاء",
  readOnly : true,
  fields : [
    {
      name : "customerName",
      label : "العميل",
    },
    {
      name : "value",
      label : "القيمة",
    }
  ],
  tableClass : function(){
    return "table table-stripped table-hover";
  },
  fieldClass : function(object, fieldName){
    return fieldName;
  },
  rowClass : function(item){
      if(!item.valid){
        return "info";
      }
      
      return "";
  },
  virtualColumns : [
    {
      name : "date",
      label : " التاريخ ",
      fn : function(customerpayement){
         var date = customerpayement.date;
         return moment(date).format('YYYY-MM-DD');
      }
    },
    
  ],
  sortKey : 'date'
}

ListView.registerList(CustomerPayments, customerPaymentList);
