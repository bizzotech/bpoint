var customerForm = {
    collection : Customers,
    name : "customer",
    class : "",
    createTitle : " اضافة عميل " ,
    readTitle : " بيانات عميل ",
    updateTitle : " تعديل بيانات عميل " ,
    fieldGroups : [
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "الاسم",
                    name : "name",
                    type : "text",
                    required : true,
                },

                {
                    class : "col-xs-6",
                    label : "رقم التيليفون",
                    name : "phoneNumber",
                    type : "text",
                }
            ]
        },

        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "العنوان",
                    name : "address",
                    type : "text",
                },
                {
                    class : "col-xs-6",
                    label : "رصيد اول فترة",
                    name : "firstTermValue",
                    type : "number",
                    visible : function(collection, object, fields, formType){

                        return formType == "create" || !object.active ;
                    }
                }
            ]
        },
        {
            class : "",
            fields : [
                {
                    class : "col-xs-12",
                    label : "طريقة الدفع",
                    name : "payMethod",
                    type : "select",
                    options :  [
                        { label : "بطاقة ائتمان", value : "1"},
                        { label : "تحويل راتب", value : "2"},
                        { label : "نقدي", value : "3"},

                  ],
                  defaultValue : "3",
                }
            ]
        }
    ],
    submitButtonContent : "حفظ",
    editButtonContent : "تعديل",
    deleteButtonContent : "حذف",
    isDeletable : function(collection, object, fields){
        return false;
    },
    customButtons : [
        {
            name : 'payCFirstTermValue',
            label : 'تسوية رصيد اول فترة',
            fn :function(collection, object, fields){
                if(object != undefined && object._id != undefined){
                    Modal.show('payCFirstTermValueModal', {object : object, fields : fields});
                }
            },
            visible : function(collection, object, fields, formType){
                return formType == "read" && object.remainigOfFirstTermValue() > 0;
            }
        }

    ],
}

FormView.registerForm(Customers, customerForm);