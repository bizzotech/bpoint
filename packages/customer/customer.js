// Write your package code here!
Customers = new Mongo.Collection("customers");

Customers.helpers({
  remainigOfFirstTermValue : function(){
    return this.firstTermValue - this.paidFromFirstTermValue;
  },
  total : function(){
    var t = this.firstTermValue || 0;
    var sales = Sales.find({customer : this._id, valid : true}).fetch();
    _.each(sales, function(sale){
      t += sale.total;
    });
    return t;
  },
  totalPayments : function(){
    var t = 0;
    var payments = CustomerPayments.find({customer : this._id, valid : true}).fetch();
    _.each(payments, function(payment){
      t += payment.value;
    });
    return t;
  },
  toBePaid : function(){
    return this.total() - this.totalPayments();
  }
});

CustomerPayments = new Mongo.Collection("customerpayments");

Meteor.startup(function () {

  Meteor.methods({
    validateCustomerPayment : function(customerpayment){
      
      //Check that payment <= toBePaid for that customer
      customer = Customers.find({_id : customerpayment.customer}).fetch()[0];
      if(customer.toBePaid() < customerpayment.value){
        if(Meteor.isClient){
            throwError( "ﻻ يمكن ان يكون المبلغ المدفوع اكبر من المستحق على العميل" );
        
        }
        return;
      }
      
      
      CustomerPayments.update(customerpayment._id, {$set : {valid : true}});
      
      var sales = Sales.find({customer : customerpayment.customer, valid : true, paid : false}).fetch();
      
      var new_payment = customerpayment.value;
      
      for(var i = 0 ; i < sales.length ; i++){
        sale = sales[i];
        var toPay = sale.total - sale.totalPaid;
        if(new_payment < toPay){
          sale.totalPaid += new_payment;
          Sales.update(sale._id, {$inc : {totalPaid : new_payment}}); 
          break;
        }else{
          Sales.update(sales[i]._id, {$inc : {totalPaid : toPay}, $set : {paid : true}});
          new_payment -= toPay;
        }
      }
      
    },
    payCFirstTermValue : function(customer_id, paymentValue){
      CustomerPayments.insert({ customer : customer_id, date : new Date(), valid : true, value : Number(paymentValue) });
    
      Customers.update(customer_id, {$inc : {paidFromFirstTermValue : Number(paymentValue)} }); 
    }

  });
  
});