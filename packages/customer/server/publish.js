ListView.registerPublication(Customers);

CustomerPayments.before.insert(function(customerpayment_id, doc){
  var customer = Customers.findOne(doc.customer);
  doc.customerName = customer.name;

  Customers.update(doc.customer, {$set : {active : true} });
});

Customers.before.insert(function(customer_id, doc){
  doc.active = false;
  doc.paidFromFirstTermValue = 0;
});