Sales = new Mongo.Collection("sales");

Sales.helpers({
  totalCost : function(){
    var total = 0;
    _.each(this.lines, function(line){
      total += line.cost;
    });
    return total;
  },
  toPay : function(){
    return this.total - this.totalPaid;
  }
});
  
Meteor.startup(function () {

  Meteor.methods({
    validateSale : function(sale){
      
      //Check if we have enough stock
      var enoughStock = true;
      _.each(sale.lines, function(line){
        console.log(line);
        product = Products.find({_id : line.product}).fetch()[0];
        if(line.qty > product.stock()){
          if(Meteor.isClient){
            throwError(" لا يوجد مخزون كافي من " + product.name );
          }
          enoughStock = false;
          return;
        }
      });
      if(!enoughStock){
       return; 
      }
      
      _.each(sale.lines, function(line){
        product = Products.find({_id : line.product}).fetch()[0];
        
        var pct = product.costTable;
        var stock = 0;
        _.each(pct, function(pl){
          stock += pl.available;
        });
        
        var needed = line.qty;
        line.cost = 0;

        
        for(var i=0; i<pct.length; i++ ){
          
          if(needed <= pct[i].available){
            line.cost += needed * pct[i].price;
            pct[i].sold += needed;
            pct[i].available -= needed;
            /*var upQ = {};
            upQ['lines.'+ pct[i].index + '.sold'] = pct[i].sold;
            upQ['lines.'+ pct[i].index + '.available'] = pct[i].available;
            Purchases.update({_id : pct[i].purchase}, {$set : upQ});
            */
            var upQ1 = {};
            upQ1['costTable.'+i+'.available'] = pct[i].available ;
            upQ1['costTable.'+i+'.sold'] = pct[i].sold ;
            Products.update({_id : product._id }, {$set : upQ1});
            
            break;
          }else {
            line.cost += pct[i].available * pct[i].price;
            pct[i].sold += pct[i].available;
            needed -= pct[i].available;
            pct[i].available = 0;
            /*var upQ = {};
            upQ['lines.'+ pct[i].index + '.sold'] = pct[i].sold;
            upQ['lines.'+ pct[i].index + '.available'] = pct[i].available;
            Purchases.update({_id : pct[i].purchase}, {$set : upQ});
            */
            var upQ1 = {};
            upQ1['costTable.'+i+'.available'] = pct[i].available ;
            upQ1['costTable.'+i+'.sold'] = pct[i].sold ;
            Products.update({_id : product._id }, {$set : upQ1});
            
            
          }
        }
        
      });

      Products.update({_id : product._id }, {
        $pull : {
          costTable : {
           available : 0 
          }
        }
      });
      
      var serial = "";
      var sale_date = moment(sale.date).format('YYMMDD');
      var last_sales = Sales.find({valid : true}, {sort : {serial : -1}, limit : 1 }).fetch();
      if(last_sales.length == 0){
        serial = sale_date + '00001';
      }else{
        var last_sale = last_sales[0];
        var last_sale_date = moment(last_sale.date).format('YYMMDD');
        if(sale_date > last_sale_date){
          serial = sale_date + '00001';
        }
        if(sale_date == last_sale_date){
          serial = Meteor.call('getNextSerial', last_sale.serial);
        }
      }
      
      
      Sales.update(sale._id, {$set : {valid : true, lines : sale.lines, serial : serial} });
      
      
    },
    
    salesValueBetween : function(from,to){
      var dateTo = moment(new Date(to)).add(1,'d').format('YYYY-MM-DD');
       var sales = Sales.find({valid : true, date : { $gte : new Date(from) , $lte : new Date(dateTo) }}).fetch();
      var total = 0;
      _.each(sales, function(sale){
        total += sale.total;
      });
      
      
      return total;
    },

    getNextSerial : function(last_serial){
      var n = last_serial.substring(6);
      var new_n_number = (parseInt(n)+1).toString();
      var new_n = "";
      for(var i = 0; i < (5 - new_n_number.length); i++){
        new_n += "0";
      }
      new_n += new_n_number;
      return last_serial.replace(n, new_n);
    },

    paySale : function(sale, toPay, paymentValue){
      if(paymentValue == toPay){
        CustomerPayments.insert({ customer : sale.customer, date : new Date(), valid : true, value : Number(paymentValue) });
    
        Sales.update(sale._id, {$inc : {totalPaid : Number(paymentValue)}, $set : {paid : true}}); 
        
      }else{
        CustomerPayments.insert({ customer : sale.customer, date : new Date(), valid : true, value : Number(paymentValue) });
    
        Sales.update(sale._id, {$inc : {totalPaid : Number(paymentValue)}}); 

      }
    }
  
  });

});
