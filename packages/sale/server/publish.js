FormView.registerPublication(Sales, [
    {
        name : "lines.product",
        collection : Products
    }
]);

SelectElement.registerPublication(Sales);
ListView.registerPublication(Sales, false, 'date');

Sales.before.insert(function(sale_id, doc){
    doc.valid = false;
    doc.totalPaid = 0;
    doc.paid = false;
    _.each(doc.lines, function(line){
        line.cost = 0;
        Products.update(line.product, {$set : {active : true}});
    });

    Customers.update(doc.customer, {$set : {active : true} });
});


Sales.before.update(function(userId, doc, fieldNames, modifier, options){
    if(modifier.$set){
      if(modifier.$set.lines){
        _.each(modifier.$set.lines, function(line){
            Products.update(line.product, {$set : {active : true}});
        });
      }
      if(modifier.$set.customer){
        Customers.update(modifier.$set.customer, {$set : {active : true} });
      }


    }


});
