Template.paySaleModal.events({
  'click #payButton' : function(event, template){
     event.preventDefault();
     var paymentValue = Number($('#paymentValue').val());
     var sale = template.data.object;
     Modal.hide('paySaleModal');
      
     var toPay = sale.toPay();
      if(paymentValue > toPay){
          throwError(" لا يمكن دفع مبلغ اكبر من قيمة الفاتورة ");
          return 
      }
      Meteor.call('paySale', sale, toPay, paymentValue)
      
      template.data.fields.set('totalPaid', sale.totalPaid + Number(paymentValue));
      template.data.fields.set('toPay', toPay - Number(paymentValue));
  }
});