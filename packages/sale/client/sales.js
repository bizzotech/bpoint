var saleList = {
  name : "sale",
  title : "قائمة فواتير بيع",
  fields : [
    {
      name : "serial",
      label : "رقم الفاتورة",
    },
    {
      name : "customerName",
      label : "العميل",
    },
    {
      name : "total",
      label : "الاجمالي",
    }
  ],
  tableClass : function(){
    return "table table-stripped table-hover";
  },
  fieldClass : function(object, fieldName){
    return fieldName;
  },
  rowClass : function(item){
      if(!item.valid){
        return "info";
      }
      if(item.toPay() == 0){
        return "success";
      }
      return "";
  },
  virtualColumns : [
    {
      name : "date",
      label : " التاريخ ",
      fn : function(sale){
         var date = sale.date;
         return moment(date).format('YYYY-MM-DD');
      }
    },
    
  ],
  sortKey : 'date'
}

ListView.registerList(Sales, saleList);
