var saleForm = {
    collection : Sales,
    name : "sale",
    class : "",
    createTitle : " تسجيل  امر بيع " ,
    readTitle : " بيانات امر بيع ",
    updateTitle : " تعديل بيانات امر بيع " ,
    fieldGroups : [
        {
            class : "",
            fields : [
                {
                    class : "col-xs-4",
                    label : "العميل",
                    name : "customer",
                    type : "object",
                    collection : "customers",
                    formName : "customer",
                    required : true,
                },

                {
                    label : "اسم العميل",
                    name : "customerName",
                    type : "text",
                    fn : function(customer){
                        if(customer != undefined && customer != "" && Customers.findOne(customer)){
                            return Customers.findOne(customer).name; 
                        }
                        return "";
                    },
                    dependOn : ["customer"],
                    style : "display : none",
                },

                {
                    class : "col-xs-4",
                    label : "التاريخ",
                    name : "date",
                    type : "date",
                    defaultValue : function(){
                        return new Date();
                    }
                },
                {
                    class : "col-xs-4",
                    label : "رقم الفاتورة",
                    name : "serial",
                    type : "text",
                    readOnly : true,
                }
            ]
        },

        {
            class : "",
            fields : [
                {
                    class : "col-xs-12",
                    label : "تم التأكيد",
                    name : "valid",
                    type : "state",
                    visible : function(){
                        return false;
                    },
                }
            ]
        },
        {
            class : "well",
            visible : function(collection, object, fields, formType){
                return formType != "read";
            },
            fields : [
                {
                    class : "col-xs-6",
                    label : "المنتج",
                    type : "object",
                    name : "product",
                    collection : "products",
                    formName : "product",
                    required : true,
                    virtual : true,
                    onChange : function(collection, fields){
                        var product_id = fields.get("product");
                        fields.set('price', 0);
                        fields.set('qty', 1);
                        if(product_id != undefined && product_id != ""){
                            Meteor.call('getProductById', product_id, function (error, product) {
                                if(product){
                                    var  price = product.price;
                                    fields.set('price', price);
                                    fields.set('qty', 1);
                                }
                            });
                            
                        }else{
                            fields.set('price', 0);
                            fields.set('qty', 1);
                        }
                        
                            
                    },
                    onEnter : function(instance){
                        var barcode = $('#productSelect input').val();
                        var product = Products.findOne({barcode : barcode});
                        if(product){
                            //alert(product.name);
                            instance.selectized.addOption(product);
                            instance.selectized.addItem(product._id);
                        }
                        
                    },
                    onInput: function(instance){
                        var barcode = $('#productSelect input').val();
                        Meteor.call('getProductByBarcode', barcode, function (error, product) {
                            if(product){
                                //alert(product.name);
                                instance.selectized.addOption(product);
                                instance.selectized.addItem(product._id);
                            }
                        });

                        
                        
                    },
                    keys : ["barcode"],
                },
                {
                    label : "اسم المنتج",
                    name : "productName",
                    type : "text",
                    virtual : true,
                    fn : function(product){
                        if(product != undefined && product != "" && Products.findOne(product)){
                            return Products.findOne(product).name; 
                        }
                        return "";
                    },
                    dependOn : ["product"],
                    style : "display : none",
                },
                {
                    class : "col-xs-3",
                    label : "الكمية",
                    name : "qty",
                    type : "number",
                    defaultValue : 1,
                    virtual : true,
                    required : true,
                },
                {
                    class : "col-xs-2",
                    label : "سعر البيع",
                    name : "price",
                    type : "number",
                    virtual : true,
                    required : true,
                },
                {
                    class : "col-xs-2",
                    label : "الاجمالي",
                    name : "subTotal",
                    type : "number",
                    virtual : true,
                    readOnly : true,
                    fn :function(qty, price){
                        if(qty != undefined && price != undefined){
                            return Number(qty) * Number(price);
                        }
                        return 0;
                    },
                    dependOn : ["qty", "price"],
                },
                {
                    class : "col-xs-2",
                    label : "التكلفة",
                    name : "cost",
                    type : "number",
                    virtual : true,
                    readOnly : true,
                    fn :function(product_id, qty){
                        if(product_id != undefined && product_id != "" && Products.findOne(product_id) && qty != undefined){
                            var product = Products.findOne(product_id);
                            qty = Number(qty);

                            if(qty > product.stock()){

                              return "غير كافي";
                            }

                            var pct = product.costTable;
                            var cost = 0;
                            var needed = qty;
                            for(var i=0; i<pct.length; i++ ){
          
                              if(needed <= pct[i].available){
                                cost += needed * pct[i].price;
                                break;
                              }else {
                                cost += pct[i].available * pct[i].price;
                                needed -= pct[i].available;
                              }
                            }

                            return cost; 
                        }
                        return "";
                    },
                    dependOn : ["product", "qty"],
                },
                {
                    class : "col-xs-2",
                    label : "اضافة",
                    type : "button",
                    name : 'addSOLine',
                    virtual : true,
                    action : function(collection, object, fields, formType, form){
                        var obj = {};
                        obj.product = fields.get("product");
                        obj.productName = fields.get("productName");
                        obj.qty = fields.get('qty');
                        obj.price = fields.get('price');
                        obj.subTotal = fields.get('subTotal') || obj.qty * obj.price;

                        var valid = true;
                        _.each(obj, function(value, name){
                            var groups = form.fieldGroups;
                            var fields = _.map(groups, function(group){
                                return group.fields;
                            });
                            fields = _.flatten(fields);
                            var field = _.filter(fields, function(field){
                                return field.name == name;
                            })[0];
                            if(field.required && value == ""){
                                valid = false;
                                $('form').find('.help-block[for=' +name + ']').text(' يجب عليك ادخال ' + field.label);

                            }
                        });
                        if(valid){
                            var lines = fields.get('lines');
                            lines.push(obj);
                            fields.set('lines', lines);
                            _.each(obj, function(value, name){
                                fields.set(name, "");
                            });
                            

                        }
                        
                        $('#productSelect input').focus();

                    }
                },
            ]
        },
        {
            class : "",
            fields : [
                {
                    class : "col-xs-12",
                    label : "تفاصيل",
                    name : "lines",
                    type : "list",
                    fields : [
                        {
                            name : "product",
                            label : "المنتج",
                            type : "object",
                            formName : "product",
                            collection : Products,
                        },
                        {
                            name : "qty",
                            label : "الكمية",
                            type : "number",
                        },
                        {
                            name : "price",
                            label : "سعر البيع",
                            type : "number",
                        },
                        {
                            name : "subTotal",
                            label : "الاجمالي",
                            type : "number",
                        }
                    ]
                }
            ]
        },
        
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "الاجمالي",
                    name : "total",
                    type : "number",
                    fn : function(lines){
                        //console.log(lines);
                        if(lines == undefined && lines == "" && lines.length == 0){
                            return 0;
                        }
                        var total = 0;
                        _.each(lines, function(line){
                            total += line.subTotal;
                        })
                        return total;
                    },
                    dependOn : ['lines'],
                    readOnly : true,
                },

                {
                    class : "col-xs-6",
                    label : "المدفوع",
                    name : "totalPaid",
                    type : "number",
                    readOnly : true,
                }
            ]
        },
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "المتبقي",
                    name : "toPay",
                    type : "number",
                    fn : function(data){
                        if(data.object)
                            return data.object.toPay();
                    },
                    dependOn : "data",
                    readOnly : true,
                    virtual : true
                }
            ]
        },
    ],
    submitButtonContent : "حفظ",
    editButtonContent : "تعديل",
    deleteButtonContent : "حذف",
    isEditable : function(collection, object, fields){
        return object.valid != true && object.valid != "true";
    },
    isDeletable : function(collection, object, fields){
        return object.valid != true && object.valid != "true";
    },
    customButtons : [
        {
            name : "confirm",
            label : "تأكيد",
            fn : function(collection, object, fields){
                if(object.lines == undefined || object.lines.length == 0 || ! object.total){
                    throwError(" لا يمكن تأكيد فاتورة فارغة " );
                    return;
                }
                for(var i=0; i<object.lines.length; i++){
                    if(! object.lines[i].subTotal){
                        throwError(" لا يمكن تأكيد فاتورة فارغة " );
                        return;
                    }
                }
                  
                if(object != undefined && object._id != undefined){
                    Meteor.call('validateSale', object);
                }
            },
            visible : function(collection, object, fields, formType){
                return formType == "read" && object.valid != true;
            }
        },
        {
            name : "pay",
            label : "دفع",
            fn : function(collection, object, fields){
                if(object != undefined && object._id != undefined){
                    Modal.show('paySaleModal', {object : object, fields : fields});
                }
            },
            visible : function(collection, object, fields, formType){
                return formType == "read" && object.toPay() > 0 && object.valid == true;
            }
        }
    ]
}

FormView.registerForm(Sales, saleForm);