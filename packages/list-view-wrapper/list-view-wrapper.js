// Write your package code here!
ListView = {};
ListView.listStructures = {}
ListView.registerList = function(collection, list){
    list.collection = collection;
    ListView.listStructures[list.name] = list;
}
ListView.constructAllRegistered = function(){
    _.each(ListView.listStructures, function(list){
        ListView._constructList(list);
    });
}
ListView._constructList = function(list){
    var listRoute = "/" + list.name + "s" ;

    var pubName = "list_" + list.collection._name;

    Router.route(listRoute, {
        action : function(){
            this.render('home');
            this.render('ListViewWrapper');
        },
        data : function(){
            return {
                listStructure : list,
            }
        }
    });
}

Template.ListViewWrapper.events({
  'click tr' : function(event, template){
    if(template.data.listStructure.hasOwnProperty('onClick')){
        template.data.listStructure.onClick(this, template);
    }else{
        Router.go("/" + template.data.listStructure.name + "/" + this._id);
    }
    
  }
});

Template.ListViewWrapper.helpers({
    title : function(){
        return Template.instance().data.listStructure.title || "لا يوجد عنوان";
    },
    isEditable : function(){
        return !(Template.instance().data.listStructure.readOnly || false);
    }
})
