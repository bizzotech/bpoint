Package.describe({
  name: 'emadshaaban:list-view-wrapper',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.1.0.2');

  api.use('underscore');
  api.use('jquery');
  api.use('templating');
  api.use('emadshaaban:list-view');
  api.use('twbs:bootstrap@3.3.5');
  
  api.addFiles('list-view-wrapper.html', ['client']);
  api.addFiles('list-view-wrapper.js', ['client']);

  api.export('ListView', ['client']);
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('emadshaaban:list-view-wrapper');
  api.addFiles('list-view-wrapper-tests.js');
});
