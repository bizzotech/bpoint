var expenseForm = {
    collection : Expenses,
    name : "expense",
    createTitle : "تسجيل مصروف",
    readTitle : " بيانات مصروف ",
    updateTitle : " تعديل بيانات مصروف " ,
    fieldGroups : [
        {
            fields : [
                {
                    class : "col-xs-6",
                    label : "نوع المصروف",
                    name : "expenseType",
                    type : "object",
                    collection : "expensetypes",
                    formName : "expensetype",
                    required : true,
                },
                {
                    class : "col-xs-6",
                    label : "التاريخ",
                    name : "date",
                    type : "date",
                    defaultValue : function(){
                        return new Date();
                    }
                },
            ]
        },
        {
            fields : [
                {
                    class : "col-xs-6",
                    label : "بيان المصروف",
                    name : "name",
                    type : "text",
                    required : true,
                },
                {
                    class : "col-xs-6",
                    label : "قيمة المصروف",
                    name : "value",
                    type : "number",
                    required : true,
                },
            ]
        },
        {
            class : "",
            fields : [
                {
                    class : "col-xs-12",
                    label : "تم التأكيد",
                    name : "valid",
                    type : "state",
                    visible : function(){
                        return false;
                    },
                }
            ]
        },
    ],
    submitButtonContent : "حفظ",
    editButtonContent : "تعديل",
    deleteButtonContent : "حذف",
    isEditable : function(collection, object, fields){
        return object.valid != true && object.valid != "true";
    },
    isDeletable : function(collection, object, fields){
        return object.valid != true && object.valid != "true";
    },
    customButtons : [
        {
            name : "confirm",
            label : "تأكيد",
            fn : function(collection, object, fields){
                if(object != undefined && object._id != undefined){
                    Meteor.call('validateExpense', object);
                }
            },
            visible : function(collection, object, fields, formType){
                return formType == "read" && object.valid != true;
            }
        }
    ]
}

FormView.registerForm(Expenses, expenseForm);