var expenseList = {
  name : "expense",
  title : "قائمة المصروفات",
  fields : [
    {
      name : "name",
      label : "بيان المصروف",
    },
    {
      name : "value",
      label : "القيمة",
    }
  ],
  tableClass : function(){
    return "table table-stripped table-hover";
  },
  fieldClass : function(object, fieldName){
    return fieldName;
  },
  rowClass : function(item){
      if(!item.valid){
        return "info";
      }
      
      return "";
  },
  virtualColumns : [
    {
      name : "type",
      label : " نوع المصروف ",
      fn : function(expense){
         var expenseType = expense.expenseType;
         console.log(expenseType);
         var type = ExpenseTypes.findOne(expenseType);
         console.log(type);
         if(type != undefined){
            return type.name;
         }
         return ""; 
      }
    },
    {
      name : "date",
      label : " التاريخ ",
      fn : function(expense){
         var date = expense.date;
         return moment(date).format('YYYY-MM-DD');
      }
    },
    
  ],
  sortKey : 'date'
}

ListView.registerList(Expenses, expenseList);
