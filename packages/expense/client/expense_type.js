var expenseTypeForm = {
    collection : ExpenseTypes,
    name : "expensetype",
    createTitle : " تسجيل نوع مصروف",
    fieldGroups : [
        {
            fields : [
                {
                    class : "col-xs-12",
                    label : "الاسم",
                    name : "name",
                    type : "text",
                    required : true,
                }
            ]
        }
    ],
    submitButtonContent : "حفظ",
    editButtonContent : "تعديل",
    deleteButtonContent : "حذف",
}

FormView.registerForm(ExpenseTypes, expenseTypeForm);