Expenses = new Mongo.Collection("expenses");

Meteor.startup(function () {
  Meteor.methods({
    validateExpense : function(expense){
      Expenses.update(expense._id, {$set : {valid : true}});
    }
  });
});

ExpenseTypes = new Mongo.Collection("expensetypes");