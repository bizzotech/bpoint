FormView.registerPublication(Expenses, [
    {
        name : 'expenseType',
        collection : ExpenseTypes

    }
]);

SelectElement.registerPublication(Expenses);
ListView.registerPublication(Expenses, [
    {
        name : 'expenseType',
        collection : ExpenseTypes

    }
], 'date');

FormView.registerPublication(ExpenseTypes);

SelectElement.registerPublication(ExpenseTypes);
