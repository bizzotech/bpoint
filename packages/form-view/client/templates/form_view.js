Template.FormView.helpers({
    hasLabel : function(field){
        return field.label != undefined && field.type != "button";
    },
    formType : function(){
        return Template.instance().data.formType;
    },
    object : function(){
        return Template.instance().data.object;
    },
    formStructure : function(){
        return Template.instance().data.formStructure;
    },
    isReadOnly : function(){
        return Template.instance().data.formType == "read";
    },
    formValues : function(){
        //console.log(Template.instance().formValues);
        return Template.instance().formValues;
    },
    isEditable : function(){
        var instance = Template.instance();
        var data = instance.data;
        var isRead = data.formType == "read" ;

        if(data.formStructure.isEditable != undefined){
            return isRead && data.formStructure.isEditable(data.formStructure.collection, data.object, instance.formValues, data.formType);
        }
        return isRead;
    },
    isSavable : function(){
        var instance = Template.instance();
        var data = instance.data;
        var createOrUpdate = data.formType == "create" || data.formType == "update";

        if(data.formStructure.isSavable != undefined){
            return createOrUpdate && data.formStructure.isSavable(data.formStructure.collection, data.object, instance.formValues, data.formType);
        }
        return createOrUpdate;
    },
    isDeletable : function(){
        var instance = Template.instance();
        var data = instance.data;
        var readOrUpdate = data.formType == "read" || data.formType == "update";

        if(data.formStructure.isDeletable != undefined){
            return readOrUpdate && data.formStructure.isDeletable(data.formStructure.collection, data.object, instance.formValues, data.formType);
        }
        return readOrUpdate;
    },
    isVisible : function(field){
        var instance = Template.instance();
        var data = instance.data;
        if(field.visible != undefined){
            return field.visible(data.formStructure.collection, data.object, instance.formValues, data.formType);
        }
        return true;
    }
});

Template.FormView.created = function(){
    var instance = this;

    //instance.object_id = instance.data.object._id;

    instance.formValues = new ReactiveDict();
    console.log(instance.data.object);
    if(instance.data.formType == "read" || instance.data.formType == "update"){
        _.each(instance.data.formStructure.fieldGroups, function(group){
            _.each(group.fields, function(field){

                if(instance.data.object.hasOwnProperty(field.name)){
                    instance.formValues.set(field.name, instance.data.object[field.name]);
                }else{
                    if(field.type == "list"){
                        instance.formValues.set(field.name, []);
                    }else{
                        instance.formValues.set(field.name, "");
                    }
                    
                }
                console.log(field.name + " -> ");
                console.log(instance.formValues.get(field.name));
            });
        });
    }else{
        _.each(instance.data.formStructure.fieldGroups, function(group){
            _.each(group.fields, function(field){

                if(field.defaultValue != undefined){
                    if((typeof field.defaultValue) == "function"){
                        instance.formValues.set(field.name, field.defaultValue());
                    }else{
                        instance.formValues.set(field.name, field.defaultValue);
                    }
                    
                }else{
                   if(field.type == "list"){
                        instance.formValues.set(field.name, []);
                    }else{
                        instance.formValues.set(field.name, "");
                    }
                }
                console.log(field.name + " -> ");
                console.log(instance.formValues.get(field.name));
               
            });
        });
    }

    instance.autorun(function(){
        _.each(instance.data.formStructure.fieldGroups, function(group){
            _.each(group.fields, function(field){

                //console.log(field);

                if(field.hasOwnProperty('fn')){

                    if(typeof(field.dependOn) == "object" && field.dependOn.hasOwnProperty('length') && field.dependOn.length > 0){
                        var relatedFields = field.dependOn;
                        var relatedFieldsVals = [];
                         _.each(relatedFields, function(fieldName){
                            var fieldVal = instance.formValues.get(fieldName) ;
                            relatedFieldsVals.push(fieldVal);
                        });
                        //relatedFieldsVals.push(instance.data);
                        var computed_val = field.fn.apply({}, relatedFieldsVals);
                        
                    }else{
                        if(typeof(field.dependOn) == "string"){
                            if(field.dependOn == "data"){
                                var computed_val = field.fn(instance.data);
                            }
                            if(field.dependOn == "fields"){
                                var computed_val = field.fn(instance.formValues);
                            }
                        }
                    }
                    instance.formValues.set(field.name, computed_val);
                }
               
            });
        });
    });

    
}


Template.FormView.events({
    'click  .submit' : function(event, template){
        // If Disconnected DO NOTHING

        if(!Meteor.status().connected){
            throwError(" برجاء الانتظار حتى يعود الاتصال ");
            return;
        }

        //console.log(template.formValues);
        var self = this;
        var data = {};
        var valid = true;
        var $form = $(template.firstNode).find('form');

        //clear help messages
        $form.find('.help-block').html("");

        var formStructure = this.formStructure;
        _.each(formStructure.fieldGroups, function(group){
            _.each(group.fields, function(field){
                if(field.virtual != undefined && field.virtual){
                    return;
                }
                var selector = "[name=" + field.name + "]"; 
                if($form.find(selector).val() != ""){
                    data[field.name] = template.formValues.get(field.name); //|| $form.find(selector).val();
                    console.log(field.name + " -> " + data[field.name]);
                    if(field.type == "number") {
                        data[field.name] = Number(data[field.name]);
                    }
                    if(field.type == "date") {
                        data[field.name] = new Date(data[field.name]);
                    }
                }else{
                    if(field.required){
                        valid = false;
                        //alert(field.name + " is required");
                        $form.find('.help-block[for=' + field.name + ']').text(' يجب عليك ادخال ' + field.label);
                    }
                }
            });
        });
    
        if(valid){
            if(self.formType == "create"){
                console.log(data);
                self.formStructure.collection.insert(data, function(error, object_id){
                    if(error){
                        console.log(error);
                        if(self.formStructure.onError != undefined){
                            self.formStructure.onError(error);
                        }
                    }else{
                        if(self.formStructure.onSuccess != undefined){
                            self.formStructure.onSuccess(object_id, data);
                        }
                    }
                });
            }
            if(self.formType == "update"){
                console.log(data);
                self.formStructure.collection.update(self.object._id, {$set : data}, function(error, result){
                    if(error){
                        console.log(error);
                        if(self.formStructure.onError != undefined){
                            self.formStructure.onError(error);
                        }
                    }else{
                        if(self.formStructure.onSuccess != undefined){
                            self.formStructure.onSuccess(self.object._id, data);
                        }
                    }
                });
            }
            
        }

    },

    'click .edit' : function(event, template){
        // If Disconnected DO NOTHING

        if(!Meteor.status().connected){
            throwError(" برجاء الانتظار حتى يعود الاتصال ");
            return;
        }
        var self = this;
        //console.log(template);
        if(self.formStructure.onEditClick != undefined){
            self.formStructure.onEditClick(template.data.object._id);
        }
    },
    'click .delete' : function(event, template){
        // If Disconnected DO NOTHING

        if(!Meteor.status().connected){
            throwError(" برجاء الانتظار حتى يعود الاتصال ");
            return;
        }
        var self = this;
        console.log(template);
        var collection = template.data.formStructure.collection;
        var object_id = template.data.object._id;
        collection.remove(object_id);
        if(self.formStructure.onDeleteClick != undefined){
            self.formStructure.onDeleteClick(template.data.object._id);
        }
    },
    'click .custom' : function(event, template){
        // If Disconnected DO NOTHING

        if(!Meteor.status().connected){
            throwError(" برجاء الانتظار حتى يعود الاتصال ");
            return;
        }
        var self = this;
        var buttons = template.data.formStructure.customButtons;
        var button = _.filter(buttons, function(b){
            return $(event.target).attr('id') == b.name;
        })[0];
        button.fn(template.data.formStructure.collection, template.data.object, template.formValues, template.data.formType);
    },
    'click .action' : function(event, template){
        event.preventDefault();
        // If Disconnected DO NOTHING

        if(!Meteor.status().connected){
            throwError(" برجاء الانتظار حتى يعود الاتصال ");
            return;
        }
        var self = this;
        var groups = template.data.formStructure.fieldGroups;
        var fields = _.map(groups, function(group){
            return group.fields;
        });
        fields = _.flatten(fields);
        var buttons = _.filter(fields, function(field){
            return field.type == "button";
        });
        var button = _.filter(buttons, function(b){
            return $(event.target).attr('id') == b.name;
        })[0];
        button.action(template.data.formStructure.collection, template.data.object, template.formValues, template.data.formType, template.data.formStructure);
        
        return false;
    }
});