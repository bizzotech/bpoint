Template.editObjectModal.helpers({
    formStructure : function(){
        var instance = Template.instance();
        var data = instance.data;
        var formName = data.formName;
        var originalForm = FormView.formStructures[formName];
        var form = $.extend(true, {}, originalForm);
        form.onSuccess = function(object_id, newDoc){
            Modal.hide('editObjectModal');
        };
        form.isDeletable = function(collection, object, fields){
            return false;
        };

        return form;
    },
    title : function(){
        var formName = Template.instance().data.formName;
        return FormView.formStructures[formName].updateTitle;
    }
});