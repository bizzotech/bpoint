Template.SelectElement.created = function(){
    var instance = this;
    instance.loaded = new ReactiveVar(0);
    instance.limit = new ReactiveVar(15);
    instance.searchQuery = "";
    instance.callback = null;
    instance.pubName = "select_" + instance.data.collection._name;

    instance.autorun(function(){
        var limit = instance.limit.get();
        var searchQuery = instance.searchQuery;

        var subscription = instance.subscribe(instance.pubName, limit, searchQuery, instance.data.field.keys);
        instance.subscription = subscription;

        if (subscription.ready()) {
            console.log("> Received "+limit+" objects. \n\n")
            instance.loaded.set(limit);
            if(instance.callback != null){
                instance.callback(instance.objects().fetch());
            }
        } else {
            console.log("> Subscription is not ready yet. \n\n");
        }

    });

    instance.objects = function() {
    
    var queryList = [];
    if(instance.data.collection.hasOwnProperty('simpleSchema')){
        _.each(instance.data.collection.simpleSchema().objectKeys(), function(key){
            var o = {};
            o[key] = {$regex : searchQuery};
            queryList.push(o);
            return instance.data.collection.find({$or : queryList}, {limit: instance.loaded.get()});
            
        }); 
    }else{
        var keys = instance.data.field.keys;
        if(keys != undefined){
            _.each(keys, function(key){
                var o = {};
                o[key] = {$regex : instance.searchQuery};
                queryList.push(o);
            });
        }
        var o = {};
        o.name = {$regex : instance.searchQuery};
        console.log(o);
        queryList.push(o);
        return instance.data.collection.find({$or : queryList}, {limit: instance.loaded.get()});
    }   
    //console.log(queryList);
   
  }



}

Template.SelectElement.helpers({
    options: function () {
        console.log(Template.instance().limit.get());
        return Template.instance().objects();
    },
    name : function(){
        return Template.instance().data.name;
    },
    hasDefaultValue : function(){
        return Template.instance().data.rawValue != undefined;
    }
});

Template.SelectElement.events({
    'click #addObject' : function(event, instance){
        //instance = instance;
        Modal.show('addObjectModal', {field : instance.data.field, selectized : instance.selectized});
    },
    'keyup': function (event, instance) {
        var field = instance.data.field;
        
        if (event.which == 13) {
            if(field.onEnter != undefined){
                field.onEnter(instance);
            }
            event.stopPropagation();
            return false;
        }
        
    },
    'input': function (event, instance) {
        var field = instance.data.field;
        
        if(field.onInput != undefined){
            field.onInput(instance);
        }
        event.stopPropagation();
        return false;
    
    },
});

Template.SelectElement.rendered = function(){
    var instance = this;
    var element_id = "#" + instance.data.name;
    var $select = $(element_id).selectize({
        valueField: '_id',
        labelField : 'name',
        searchField : 'name',
        options : [],
        render : {
            option : function(item, escape){
                return "<div>" + item.name + "</div>";
            }
        },
        load : function(query, callback) {
            instance.callback = callback;
            instance.searchQuery = query;
            instance.limit.set(0);
            instance.limit.set(15);
        }
    });
    instance.selectized = $select[0].selectize;
    var defaultOptionId = instance.data.rawValue;
    if(defaultOptionId != undefined && defaultOptionId != ""){
        //alert(defaultOptionId);
        var selectedObject = instance.data.collection.findOne(defaultOptionId);
        instance.selectized.addOption(selectedObject);
        instance.selectized.addItem(defaultOptionId);
    }

    instance.autorun(function(){
        var data = instance.data;
        var val = data.formValues.get(data.field.name);
        if(val == undefined || val == ""){
            instance.selectized.clearOptions();
        }
    });

}