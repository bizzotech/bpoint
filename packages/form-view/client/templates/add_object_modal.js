Template.addObjectModal.helpers({
    formStructure : function(){
        var instance = Template.instance();
        var data = instance.data;
        var formName = data.field.formName;
        var originalForm = FormView.formStructures[formName];
        var form = $.extend(true, {}, originalForm);
        form.onSuccess = function(object_id, newDoc){
            Modal.hide('addObjectModal');
            var newObject = newDoc;
            newObject['_id'] = object_id;
            data.selectized.addOption(newObject);
            data.selectized.addItem(object_id);
        }

        return form;
    },
    title : function(){
        var formName = Template.instance().data.field.formName;
        return FormView.formStructures[formName].createTitle;
    }
});