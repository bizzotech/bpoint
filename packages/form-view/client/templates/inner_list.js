Template.InnerList.helpers({
    isReadOnly : function(){
        return Template.instance().data.isReadOnly;
    },
    listObjects: function () {
        var instance = Template.instance();
        var data = instance.data;
        var list = data.formValues.get(data.field.name);
        var indexedList = [];
        _.each(list, function(object, index){
            object.index = index;
            indexedList.push(object);
        });
        //console.log(indexedList);
        return indexedList;
    },
    fields : function(){
        return Template.instance().data.field.fields;
    },
    getValue : function(object, field){
        if(field.type == "object"){
            console.log(field.collection);
            var objects = field.collection.find(object[field.name]).fetch();
            if(objects.length > 0){
                return "<a class='object-details' data-id='" + objects[0]._id + "' data-form-name='" + field.formName + "' data-collection-name='" + field.collection._name + "' href='#'>" + objects[0].name + "</a>";
            }
        }
        return object[field.name];
    },
    notEmptyList : function(){
        var instance = Template.instance();
        var data = instance.data;
        return data.formValues.get(data.field.name).length || "";
    }
});

Template.InnerList.created = function(){

    var instance = this;
    var formValues = instance.data.formValues;
    var list = instance.data.field;
    instance.autorun(function(){
        _.each(list.fields, function(field){
            if(field.type == "object"){
                var pubName = 'inner_' + field.collection._name;
                var lines = formValues.get(list.name);
                var ids = _.map(lines, function(line){
                    return line[field.name];
                });
                instance.subscribe(pubName, ids);
            }
        })
        
    });
}

Template.InnerList.events({
    'click .remove-item' : function(event, template){
        event.preventDefault();
        var index = $(event.target).attr('index');
        var lines = template.data.formValues.get(template.data.field.name);
        var newLines = [];
        _.each(lines, function(line, i){
             if(i != index){
                newLines.push(line);
             }
        })
        template.data.formValues.set(template.data.field.name, newLines);
        return false;
    },
    'click .object-details' : function(event, instance){
        //instance = instance;
        var objectId = $(event.target).attr('data-id');
        var collectionName = $(event.target).attr('data-collection-name');
        var collection = Mongo.Collection.get(collectionName);
        var formName = $(event.target).attr('data-form-name');
        var object = collection.findOne(objectId);
        Modal.show('editObjectModal', {formName : formName, object : object});
    },
});