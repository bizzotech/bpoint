Template.FormElement.helpers({
    isReadOnly : function(){
        var data = Template.instance().data;
        //alert(Template.instance().data.formType);
        var editable = true;
        if(data.field.hasOwnProperty('isEditable')){
            editable = data.field.isEditable(data.field.collection, data.object, Template.instance().formValues, data.formType);
        }
        return data.formType == "read" ||  data.field.readOnly || !editable ;
    },
    formValues : function(){
        return Template.instance().data.formValues;
    },
    rawValue : function(){
        var data = Template.instance().data;
        return  data.formValues.get(data.field.name);
    },
    value : function(){
        var data = Template.instance().data;
        if(data.field.type == "select"){
            var val =  data.formValues.get(data.field.name);
            var option = _.filter(data.field.options, function(option){
                return option.value == val;
            })[0];
            return option.label;
        }
        if(data.field.type == "date"){
            var val =  data.formValues.get(data.field.name);
            return moment(val).format('YYYY-MM-DD') || "";
        }

        if(data.field.type == "number"){
            var val =  data.formValues.get(data.field.name);
            return Number(Number(val).toFixed(2)) || 0;
        }

        if(data.field.type == "object"){
            var object_id =  data.formValues.get(data.field.name);
            console.log(object_id);
            if(object_id != undefined && object_id != ""){
                var collection = Mongo.Collection.get(Template.instance().data.field.collection);
                var object = collection.findOne(object_id);
                return object.name || "No Name";
            }
            return "";
            
        }
        return  data.formValues.get(data.field.name) || "";
    },
    isButton : function(){
        return Template.instance().data.field.type == "button";
    },
    isList : function(){
        return Template.instance().data.field.type == "list";
    },
    isSelect : function(){
        return Template.instance().data.field.type == "select";
    },
    isObject : function(){
        return Template.instance().data.field.type == "object";
    },
    collection : function(){
        return Mongo.Collection.get(Template.instance().data.field.collection);
    },
    isInput : function(){
        var type = Template.instance().data.field.type ;
        return type != "list" && type != "button" && type != "object" && type != "select"  && type != "state";
    },
    isState : function(){
        var type = Template.instance().data.field.type ;
        return  type == "state";
    },
    isBarcode : function(){
        var type = Template.instance().data.field.type ;
        return  type == "barcode";
    },
    isDefault : function(value){
        return value == Template.instance().data.field.defaultValue;
    }
});

Template.FormElement.events({
    'input input' : function(event, template){
        var value = $(event.target).val();
        if($(event.target).attr('type') == 'date'){
            value = moment(value).toString();
        }
        if($(event.target).attr('type') == 'number'){
            value = Number(value);
        }
        if($(event.target).attr('id') ==  template.data.field.name){
            template.data.formValues.set(template.data.field.name, value);
        }
        if(template.data.field.hasOwnProperty('onChange')){
            template.data.field.onChange(template.data.collection, template.data.formValues);
        }
    },
    'change select' : function(event, template){
        var value = $(event.target).val();
        if($(event.target).attr('id') ==  template.data.field.name){
            template.data.formValues.set(template.data.field.name, value);
        }
        if(template.data.field.hasOwnProperty('onChange')){
            template.data.field.onChange(template.data.collection, template.data.formValues);
        }
    },
});