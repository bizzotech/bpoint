Template.CreateFormView.helpers({
    title : function(){
        return Template.instance().data.formStructure["createTitle"];
    }
});

Template.ReadFormView.helpers({
    title : function(){
        return Template.instance().data.formStructure["readTitle"];
    }
});

Template.UpdateFormView.helpers({
    title : function(){
        return Template.instance().data.formStructure["updateTitle"];
    }
});

FormView = {};
FormView.formStructures = {};

FormView.registerForm = function(collection, form){
    form.collection = collection;
    FormView.formStructures[form.name] = form;
}
FormView.constructAllRegistered = function(){
    _.each(FormView.formStructures, function(form){
        FormView._constructForm(form);
    });
}

FormView._constructForm = function(form){
    var collection = form.collection;
    var newRoute = "/" + form.name + "/new" ;
    var viewRoute = "/" + form.name + "/:_id";
    var editRoute = "/" + form.name + "/edit/:_id";

    var pubName = "form_" + collection._name;


    Router.route(newRoute, {
        action : function(){
            this.render('CreateFormView');
        },
        data : function(){
            return {
                formStructure : form,
                object : undefined,
            }
        }
    });

    Router.route(viewRoute, {
        waitOn : function(){
            return [
                Meteor.subscribe(pubName, this.params._id),
            ]
        },
        action : function(){
            this.render('ReadFormView', {
                data : function(){
                    return  {
                        formStructure : form,
                        object : collection.findOne({_id : this.params._id}),
                    }
                }
            })
        }
    });

    Router.route(editRoute, {
        waitOn : function(){
            return [
                Meteor.subscribe(pubName, this.params._id),
            ]
        },
        action : function(){
            this.render('UpdateFormView', {
                data : function(){
                    return  {
                        formStructure : form,
                        object : collection.findOne({_id : this.params._id}),
                    }
                }
            })
        }
    });

    if(form.onSuccess == undefined){
        form.onSuccess = function(object_id){
            Router.go( "/" + form.name + "/" + object_id);
        }
    }

    if(form.onEditClick == undefined){
        form.onEditClick = function(object_id){
            Router.go( "/" + form.name + "/edit/" + object_id);
        }
    }

    if(form.onDeleteClick == undefined){
        form.onDeleteClick = function(object_id){
            Router.go( "/" + form.name + "s" );
        }
    }
    

}