FormView = {};

FormView.publications = {};
FormView.registerPublication = function(collection, _children){
    var pubName = "form_" + collection._name;
    var children = _children || [];
    if(FormView.publications.hasOwnProperty(pubName)){
        var oldChildren = FormView.publications[pubName].children || [];
        var updatedChildren = oldChildren.concat(children);
        FormView.publications[pubName].children = updatedChildren;
        
    }else{
        FormView.publications[pubName] = {
            collection : collection,
            children : children
        }
    }
    
}

FormView.publishAll = function(){
    _.each(FormView.publications, function(pub, pubName){
        FormView.publish(pub.collection, pub.children, pubName);
    })
}

FormView.publish = function(collection, _children, _pubName){
    var pubName = _pubName || "form_" + collection._name;
    
    var children = [];
    _.each(_children, function(key){
        var child = {
            find : function(object){
                if(key.name.indexOf('.') > -1){
                    var firstLevelKey = key.name.split(".")[0];
                    var secondLevelKey = key.name.split(".")[1];
                    var ids = [];
                    _.each(object[firstLevelKey], function(ch){
                        ids.push(ch[secondLevelKey]);
                    });
                    return key.collection.find({_id : {$in : ids}});
                }
                if(key.hasOwnProperty('relatedName')){
                    var query = {};
                    query[key.relatedName] = object[key.name];
                    return key.collection.find(query);
                }
                return key.collection.find(object[key.name]);
            }
        }
        children.push(child);
    });


    Meteor.publishComposite(pubName, function (object_id) {
        return {
            find : function(){
                return collection.find({_id : object_id});
            },
            children : children
        }

        
    });
}



SelectElement = {};
SelectElement.publications = {};
SelectElement.registerPublication = function(collection, _children){
    var pubName = "select_" + collection._name;
    var children = _children || [];
    if(SelectElement.publications.hasOwnProperty(pubName)){
        var oldChildren = SelectElement.publications[pubName].children || [];
        var updatedChildren = oldChildren.concat(children);
        SelectElement.publications[pubName].children = updatedChildren;
        
    }else{
        SelectElement.publications[pubName] = {
            collection : collection,
            children : children
        }
    }
    
}

SelectElement.publishAll = function(){
    _.each(SelectElement.publications, function(pub, pubName){
        SelectElement.publish(pub.collection, pub.children, pubName);
    })
}
SelectElement.publish = function(collection, _children, _pubName){
    var pubName = _pubName || "select_" + collection._name;
    
    var children = [];
    _.each(_children, function(key){
        var child = {
            find : function(object){
                if(key.hasOwnProperty('relatedName')){
                    var query = {};
                    query[key.relatedName] = object[key.name];
                    return key.collection.find(query);
                }
                return key.collection.find(object[key.name]);
            }
        }
        children.push(child);
    });

    Meteor.publishComposite(pubName, function (l, sQ, keys) {
        var limit = l || 15;
        var searchQuery = sQ || "";

        var queryList = [];
        if(collection.hasOwnProperty('simpleSchema')){
            _.each(collection.simpleSchema().objectKeys(), function(key){
                var o = {};
                o[key] = {$regex : searchQuery};
                queryList.push(o);
                return {
                    find : function(){
                        return collection.find({$or : queryList}, {limit: limit});
                    },
                    children : children,
                }
                
            }); 
        }else{
            if(keys != undefined){
                _.each(keys, function(key){
                    var o = {};
                    o[key] = {$regex : searchQuery};
                    queryList.push(o);
                });
            }
            var o = {};
            o.name = {$regex : searchQuery};
            queryList.push(o);
            return {
                find : function(){
                    return collection.find({$or : queryList}, {limit: limit});
                },
                children : children,
            }
        }
        

        

        
    
    });
}

InnerList = {};
InnerList.publications = {};
InnerList.registerPublication = function(collection, _children){
    var pubName = "inner_" + collection._name;
    var children = _children || [];
    if(InnerList.publications.hasOwnProperty(pubName)){
        var oldChildren = InnerList.publications[pubName].children || [];
        var updatedChildren = oldChildren.concat(children);
        InnerList.publications[pubName].children = updatedChildren;
        
    }else{
        InnerList.publications[pubName] = {
            collection : collection,
            children : children
        }
    }
    
}

InnerList.publishAll = function(){
    _.each(InnerList.publications, function(pub, pubName){
        InnerList.publish(pub.collection, pub.children, pubName);
    })
}

InnerList.publish = function(collection, _children, _pubName){
    var pubName = _pubName || "inner_" + collection._name;
    
    var children = [];
    _.each(_children, function(key){
        var child = {
            find : function(object){
                if(key.hasOwnProperty('relatedName')){
                    var query = {};
                    query[key.relatedName] = object[key.name];
                    return key.collection.find(query);
                }
                return key.collection.find(object[key.name]);
            }
        }
        children.push(child);
    });

    Meteor.publishComposite(pubName, function (ids) {
        return {
            find : function(){
                return collection.find({_id : {$in : ids}});
            },
            children : children,
        }

        
    
    });
}