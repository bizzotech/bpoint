Package.describe({
  name: 'emadshaaban:form-view',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.1.0.2');

  api.use('templating');
  api.use('underscore');
  api.use('reactive-dict');
  api.use('twbs:bootstrap@3.3.5');
  api.use('dburles:mongo-collection-instances@0.3.4');
  api.use('asmahan:barcode@0.0.1');
  api.use('255kb:meteor-status@1.4.2');

  api.addFiles('client/templates/form_view.html', ['client']);
  api.addFiles('client/templates/form_view_wrapper.html', ['client']);
  api.addFiles('client/templates/form_element.html', ['client']);
  api.addFiles('client/templates/select_element.html', ['client']);
  api.addFiles('client/templates/inner_list.html', ['client']);
  api.addFiles('client/templates/add_object_modal.html', ['client']);
  api.addFiles('client/templates/edit_object_modal.html', ['client']);

  api.addFiles('client/templates/form_view.css', ['client']);

  api.addFiles('client/templates/form_view.js', ['client']);
  api.addFiles('client/templates/form_view_wrapper.js', ['client']);
  api.addFiles('client/templates/form_element.js', ['client']);
  api.addFiles('client/templates/select_element.js', ['client']);
  api.addFiles('client/templates/inner_list.js', ['client']);
  api.addFiles('client/templates/add_object_modal.js', ['client']);
  api.addFiles('client/templates/edit_object_modal.js', ['client']);

  api.addFiles('server/publish.js' , ['server']);
  
  api.export('FormView');
  api.export('SelectElement');
  api.export('InnerList');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('emadshaaban:form-view');
  api.addFiles('form-view-tests.js');
});
