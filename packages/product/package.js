Package.describe({
  name: 'bpoint:product',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.1.0.2');

  api.use(['minimongo', 'mongo-livedata', 'mrt:moment'], 'client');
  api.use('mongo');
  api.use('reywood:publish-composite', ['server']);
  api.use('emadshaaban:form-view');
  api.use('emadshaaban:list-view');
  api.use('emadshaaban:list-view-wrapper');

  api.addFiles('product.js');

  api.addFiles('client/product.js', ['client']);
  api.addFiles('client/products.js', ['client']);
  
  api.addFiles('server/publish.js', ['server']);

  api.export('Products');
  
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('bpoint:product');
  api.addFiles('product-tests.js');
});
