var productForm = {
    collection : Products,
    name : "product",
    class : "",
    createTitle : "اضافة منتج",
    readTitle : " بيانات منتج ",
    updateTitle : " تعديل بيانات منتج " ,
    fieldGroups : [
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "الاسم",
                    name : "name",
                    type : "text",
                    tag : "input",
                    required : true,
                    isEditable : function(collection, object, fields, formType){
                        return formType == "create" || !object.active;
                    }
                },

                {
                    class : "col-xs-6",
                    label : "سعر البيع",
                    name : "price",
                    type : "number",
                    tag : "input",
                    required : true,
                    defaultValue : 0,
                }
            ]
        },

        {
            class : "",
            fields : [
                {
                    class : "col-xs-12",
                    label : "الباركود",
                    name : "barcode",
                    type : "barcode",
                    tag : "input",
                    defaultValue : function(){
                        return  Math.random().toString(36).substr(2, 9);
                    },
                    isEditable : function(collection, object, fields, formType){
                        return formType == "create" || !object.active;
                    }
                }
            ]
        },
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "مخزون اول الفترة",
                    name : "firstTermStock",
                    type : "number",
                    visible : function(collection, object, fields, formType){

                        return formType == "create" || !object.active ;
                    }
                },
                {
                    class : "col-xs-6",
                    label : "سعر التكلفة",
                    name : "stockPrice",
                    type : "number",
                    visible : function(collection, object, fields, formType){
                        return formType == "create" || !object.active ;
                    }
                }

            ]
        }
    ],
    submitButtonContent : "حفظ",
    editButtonContent : "تعديل",
    deleteButtonContent : "حذف",
    isDeletable : function(collection, object, fields){
        return false;
    },

    customButtons : [
        {
            name : 'printBarcode',
            label : 'طباعة باركود',
            fn :function(collection, object, fields){
                var dataUrl = document.getElementById('barcode-canvas').toDataURL(); 
                var windowContent = '<!DOCTYPE html>';
                windowContent += '<html>'
                windowContent += '<head><title>طباعة باركود</title></head>';
                windowContent += '<body>'
                windowContent += '<div style="text-align : center;">'
                windowContent += '<img style="margin-bottom : -45px;" src="' + dataUrl + '">';
                windowContent += '<div>'
                //windowContent += '<p style="text-align : center; font-size:20px; letter-spacing : 6px; margin-bottom : -20px;"> ' + object.barcode + ' </p>'
                windowContent += '<p style="text-align : center; font-size:20px; letter-spacing : 1px; margin-bottom : -20px;"> ' + object.price.toFixed(2) + ' </p>'
                windowContent += '<p style="text-align : center; font-size:15px;"> ' + object.name + ' </p>'
                windowContent += '</body>';
                windowContent += '</html>';
                var printWin = window.open();
                printWin.document.open();
                printWin.document.write(windowContent);
                printWin.document.close();
                printWin.focus();
                printWin.print();
                printWin.close();
            },
            visible : function(collection, object, fields, formType){
                return formType != "create";
            }
        }

    ],
    onError : function(error){
        throwError(" خطأ اثناء التسجيل، تأكد من عدم تكرار الباركود ");
    }
}

FormView.registerForm(Products, productForm);