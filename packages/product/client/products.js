var productList = {
  name : "product",
  title : "قائمة المنتجات",
  fields : [
    {
      name : "name",
      label : "الاسم",
    },
    {
      name : "price",
      label : "سعر البيع",
    },
    {
      name : "barcode",
      label : "الباركود",
    }
  ],
  rowClass : function(item){
    if(!item.stock() > 0){
      return "danger";
    }

    return "";
  },
  tableClass : function(){
    return "table table-stripped table-hover";
  },
  fieldClass : function(object, fieldName){
    return fieldName;
  },
  virtualColumns : [
    {
      name : "stock",
      label : "اجمالي المخزون",
      fn : function(product){
        var val = product.stock();
        return Number(Number(val).toFixed(2)) || 0;
      },
    },
    {
      name : "stockValue",
      label : "اجمالي قيمة المخزون",
      fn : function(product){
        var val = product.stockValue();
        return Number(Number(val).toFixed(2)) || 0; 
      },
    }
  ]
}

ListView.registerList(Products, productList);
