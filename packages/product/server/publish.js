ListView.registerPublication(Products);
FormView.registerPublication(Products);
SelectElement.registerPublication(Products);
InnerList.registerPublication(Products);

Products.before.insert(function(product_id, doc) {
  doc.costTable = [{
    available: doc.firstTermStock || 0,
    price: doc.stockPrice || 0,
    sold: 0,
  }];
  doc.active = false;
});

Products._ensureIndex({
  barcode: 1
}, {
  unique: 1
});

Products.before.update(function(userId, doc, fieldNames, modifier, options) {
  if (!doc.active && fieldNames.indexOf('active') == -1) {
    modifier.$set = modifier.$set || {};
    modifier.$set.costTable = [{
      available: modifier.$set.firstTermStock == undefined && doc.firstTermStock ||
        modifier.$set.firstTermStock,
      price: modifier.$set.stockPrice == undefined && doc.stockPrice ||
        modifier.$set.stockPrice,
      sold: 0,
    }];
    if (modifier.$set.costTable[0].available == undefined)
      modifier.$set.costTable[0].available = 0;
    if (modifier.$set.costTable[0].price == undefined)
      modifier.$set.costTable[0].price = 0;
  }

});

Meteor.startup(function () {
    Meteor.methods({
        getProductById : function(product_id){
            return product = Products.findOne(product_id);
        },
        getProductByBarcode : function(barcode){
            return product = Products.findOne({barcode : barcode});
        }
    })
});
