Products = new Mongo.Collection("products");

Products.helpers({
  stock: function() {
    var total = 0;
    _.each(this.costTable, function(line){
      total += line.available;
    });
    return total;
  },
  stockValue: function() {
    var total = 0;
    _.each(this.costTable, function(line){
      total += line.price * line.available;
    });
    return Math.round(total);
  }
});