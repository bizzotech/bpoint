Purchases = new Mongo.Collection("purchases");
OperationExpenses = new Mongo.Collection("operationexpenses");

Purchases.helpers({
  totalCost : function(){
    var total = 0;
    _.each(this.lines, function(line){
      total += line.cost;
    });
    return total;
  },
  toPay : function(){
    return this.total - this.totalPaid;
  }
});

Meteor.startup(function () {

  Meteor.methods({
    validatePurchase : function(purchase){
      Purchases.update(purchase._id, {$set : {valid : true} });
      
      
      var totalQty = 0;
      
      _.each(purchase.lines, function(line){
        totalQty += line.qty;
      });
      var productExpense = purchase.totalExpenses / totalQty;
      
      console.log("Product Expense " + productExpense);
      
      _.each(purchase.lines, function(line, i){
        Products.update({_id : line.product}, {
          $push : {
            costTable : {
              available : line.qty,
              price : line.price + productExpense,
              purchase : purchase._id,
              index : i,
              sold : 0
            }
          }
        });

      });
          
    },

    payPurchase : function(purchase, toPay, paymentValue){
      if(paymentValue == toPay){
        SupplierPayments.insert({ supplier : purchase.supplier, date : new Date(), valid : true, value : Number(paymentValue) });
    
        Purchases.update(purchase._id, {$inc : {totalPaid : Number(paymentValue)}, $set : {paid : true}}); 
        
      }else{
        SupplierPayments.insert({ supplier : purchase.supplier, date : new Date(), valid : true, value : Number(paymentValue) });
    
        Purchases.update(purchase._id, {$inc : {totalPaid : Number(paymentValue)}}); 

      }
    }
  });

});
