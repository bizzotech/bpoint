FormView.registerPublication(Purchases, [
    {
        name : "lines.product",
        collection : Products
    },
    {
        name : "operationExpenses.expense",
        collection : OperationExpenses
    }
]);

SelectElement.registerPublication(Purchases);
ListView.registerPublication(Purchases, false, 'date');

FormView.registerPublication(OperationExpenses);
SelectElement.registerPublication(OperationExpenses);

Purchases.before.insert(function(purchase_id, doc){
    doc.valid = false;
    doc.totalPaid = 0;
    doc.paid = false;
    _.each(doc.lines, function(line){
        Products.update(line.product, {$set : {active : true} });
    });

    Suppliers.update(doc.supplier, {$set : {active : true} });
})


Purchases.before.update(function(userId, doc, fieldNames, modifier, options){
    if(modifier.$set){
      if(modifier.$set.lines){
        _.each(modifier.$set.lines, function(line){
            Products.update(line.product, {$set : {active : true}});
        });
      }
      if(modifier.$set.supplier){
        Suppliers.update(modifier.$set.supplier, {$set : {active : true} });
      }


    }


});
