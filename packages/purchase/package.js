Package.describe({
  name: 'bpoint:purchase',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.1.0.2');

  api.use(['minimongo', 'mongo-livedata', 'mrt:moment', 'templating'], 'client');
  api.use('mongo');
  api.use('reywood:publish-composite', ['server']);
  api.use('emadshaaban:form-view');
  api.use('emadshaaban:list-view');
  api.use('emadshaaban:list-view-wrapper');
  api.use('bpoint:product')

  api.addFiles('purchase.js');

  api.addFiles('client/pay_purchase_modal.html', ['client']);
  
  api.addFiles('client/purchase.js', ['client']);
  api.addFiles('client/purchases.js', ['client']);
  api.addFiles('client/pay_purchase_modal.js', ['client']);
  api.addFiles('client/operation_expense.js', ['client']);

  api.addFiles('server/publish.js', ['server']);

  api.export('Purchases');  

});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('bpoint:purchase');
  api.addFiles('purchase-tests.js');
});
