var operationExpenseForm = {
    collection : OperationExpenses,
    name : "operationexpense",
    createTitle : " تسجيل نوع مصروف تشغيلي",
    fieldGroups : [
        {
            fields : [
                {
                    class : "col-xs-12",
                    label : "الاسم",
                    name : "name",
                    type : "text",
                    required : true,
                }
            ]
        }
    ],
    submitButtonContent : "حفظ",
    editButtonContent : "تعديل",
    deleteButtonContent : "حذف",
}

FormView.registerForm(OperationExpenses, operationExpenseForm);