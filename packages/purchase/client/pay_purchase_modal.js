Template.payPurchaseModal.events({
  'click #payButton' : function(event, template){
     event.preventDefault();
     var paymentValue = Number($('#paymentValue').val());
     var purchase = template.data.object;
     Modal.hide('payPurchaseModal');
      
     var toPay = purchase.toPay();
      if(paymentValue > toPay){
          throwError(" لا يمكن دفع مبلغ اكبر من قيمة الفاتورة ");
          return 
      }
      Meteor.call('payPurchase', purchase, toPay, paymentValue)
      
      template.data.fields.set('totalPaid', purchase.totalPaid + Number(paymentValue));
      template.data.fields.set('toPay', toPay - Number(paymentValue));
  }
});