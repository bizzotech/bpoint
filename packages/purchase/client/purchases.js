var purchaseList = {
  name : "purchase",
  title : "قائمة فواتير شراء",
  fields : [
    {
      name : "serial",
      label : "رقم الفاتورة",
    },
    {
      name : "supplierName",
      label : "المورد",
    },
    {
      name : "totalWithExpenses",
      label : "الاجمالي",
    }
  ],
  tableClass : function(){
    return "table table-stripped table-hover";
  },
  fieldClass : function(object, fieldName){
    return fieldName;
  },
  rowClass : function(item){
      if(!item.valid){
        return "info";
      }
      if(item.toPay() == 0){
        return "success";
      }
      return "";
  },
  virtualColumns : [
    {
      name : "date",
      label : " التاريخ ",
      fn : function(purchase){
         var date = purchase.date;
         return moment(date).format('YYYY-MM-DD');
      }
    },
    
  ],
  sortKey : 'date'
}

ListView.registerList(Purchases, purchaseList);
