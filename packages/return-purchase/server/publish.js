FormView.registerPublication(Purchases, [
    {
        name : '_id',
        collection : PurchaseReturns,
        relatedName : 'purchase',
    }
]);

FormView.registerPublication(SupplierPayments, [
    {
        name : 'supplier',
        collection : PurchaseReturns,
        relatedName : 'supplier',
    }
]);

ListView.registerPublication(Purchases, [
    {
        name : '_id',
        collection : PurchaseReturns,
        relatedName : 'purchase',
    }
]);

ListView.registerPublication(SupplierPayments, [
    {
        name : 'supplier',
        collection : PurchaseReturns,
        relatedName : 'supplier',
    }
]);

FormView.registerPublication(PurchaseReturns);

PurchaseReturns.before.insert(function(purchaseReturnId, doc){
    doc.valid = false;
  
});

ListView.registerPublication(Suppliers, [
    {
        name : '_id',
        collection : PurchaseReturns,
        relatedName : 'supplier',
    }
]);

SelectElement.registerPublication(Suppliers, [
    {
        name : '_id',
        collection : PurchaseReturns,
        relatedName : 'supplier',
    }
]);