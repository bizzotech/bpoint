PurchaseReturns = new Mongo.Collection("purchasereturns");

PurchaseReturns.helpers({
  totalCost : function(){
    var total = 0;
    _.each(this.lines, function(line){
      total += line.cost;
    });
    return total;
  },
});

Purchases.helpers({
    returns : function(){
        return PurchaseReturns.find({purchase : this._id});
    },
    returnValues : function(){
        var returns = PurchaseReturns.find({purchase : this._id});
        var returnValues = [];
        _.each(this.lines, function(line){
            returnValues.push(0);
        });
        if(returns.count() > 0){
            _.each(returns.fetch(), function(_return){
                _.each(_return.lines, function(line, i){
                    returnValues[i] += line.qty;
                });
            });
        }
        return returnValues;
    },
    returnTotal : function(){
        var returns = PurchaseReturns.find({purchase : this._id, valid : true});
        var total = 0;
        _.each(returns.fetch(), function(_return){
            total += _return.total;
        });
        return total;
    },
    toPay : function(){
        return this.total - this.totalPaid - this.returnTotal();
    }
});

Suppliers.helpers({
    totalPurchaseReturns : function(){
        var returns = PurchaseReturns.find({supplier : this._id, valid : true});
        var total = 0;
        _.each(returns.fetch(), function(_return){
            total += _return.total;
        });
        return total;
    },
    toBePaid : function(){
        return this.total() - this.totalPayments() - this.totalPurchaseReturns();
    }
})

Meteor.startup(function () {
    Meteor.methods({
        returnPurchase : function(purchase, returnLines){
            //alert("Return" + purchase.supplierName);
            //console.log(returnLines);
            var total = 0;
            _.each(returnLines, function(line){
                total += line.subTotal;
            });
            PurchaseReturns.insert({
                purchase : purchase._id,
                supplier : purchase.supplier,
                supplierName : purchase.supplierName,
                date : new Date(),
                lines : returnLines,
                total : total,
            });
        },
        validatePurchaseReturn : function(purchaseReturn){
            PurchaseReturns.update(purchaseReturn._id, {$set : {valid : true} } );


            //Check if we have enough stock
            var enoughStock = true;
            _.each(purchaseReturn.lines, function(line){
                product = Products.find({_id : line.product}).fetch()[0];
                if(line.qty > product.stock()){
                    if(Meteor.isClient){
                        throwError(" لا يوجد مخزون كافي من " + product.name );
                    }
                    enoughStock = false;
                    return;
                }
            });
            if(!enoughStock){
                return; 
            }


            _.each(purchaseReturn.lines, function(line){
                product = Products.find({_id : line.product}).fetch()[0];
                
                var pct = product.costTable;
                var stock = 0;
                _.each(pct, function(pl){
                  stock += pl.available;
                });
                
                var needed = line.qty;
                line.cost = 0;

                
                for(var i=0; i<pct.length; i++ ){
                  
                  if(needed <= pct[i].available){
                    line.cost += needed * pct[i].price;
                    pct[i].sold += needed;
                    pct[i].available -= needed;
                    
                    var upQ1 = {};
                    upQ1['costTable.'+i+'.available'] = pct[i].available ;
                    upQ1['costTable.'+i+'.sold'] = pct[i].sold ;
                    Products.update({_id : product._id }, {$set : upQ1});
                    
                    break;
                  }else {
                    line.cost += pct[i].available * pct[i].price;
                    pct[i].sold += pct[i].available;
                    needed -= pct[i].available;
                    pct[i].available = 0;
                    
                    var upQ1 = {};
                    upQ1['costTable.'+i+'.available'] = pct[i].available ;
                    upQ1['costTable.'+i+'.sold'] = pct[i].sold ;
                    Products.update({_id : product._id }, {$set : upQ1});
                    
                    
                  }
                }
            });
                
            
            
            Products.update({_id : product._id }, {
              $pull : {
                costTable : {
                 available : 0 
                }
              }
            });

            
        }
    })
});