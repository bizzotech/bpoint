Template.payReturnPurchaseModal.events({
  'click #payButton' : function(event, template){
     event.preventDefault();
     var paymentValue = 0 - Number($('#paymentValue').val());
     var purchase = template.data.object;
     Modal.hide('payReturnPurchaseModal');
      
     var toPay = purchase.toPay();
      if(paymentValue < toPay){
          throwError(" لا يمكن رد مبلغ اكبر من اجمالي المردودات ");
          return 
      }
      Meteor.call('payPurchase', purchase, toPay, paymentValue)
      
      template.data.fields.set('totalPaid', purchase.totalPaid + Number(paymentValue));
      template.data.fields.set('toPay', purchase.toPay() - Number(paymentValue));
      
  }
});