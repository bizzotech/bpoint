Template.returnPurchaseModal.events({
  'click #returnButton' : function(event, template){
     event.preventDefault();
     //var paymentValue = $('#paymentValue').val();
     Modal.hide('payPurchaseModal');
     
     var purchase = template.data.object;
     var lines = template.data.fields.get('lines');
     var returnedLines = [];
     var returnedValues = purchase.returnValues();
     //console.log($('.return-line'));
     var  valid = true;
     $('.return-line').each(function(i){
        var line = lines[i];
        var qty = Number($(this).find('.qty').val());
        if((line.qty - returnedValues[i]) < qty){
          valid = false;
        }else{
          var cost = line.cost / line.qty * qty;
          var subTotal = line.price * qty;
          var returnedLine = {
            product : line.product,
            productName : line.productName,
            price : line.price,
            qty : qty,
            subTotal : subTotal,
            cost : cost,
          }
          returnedLines.push(returnedLine);
        }

     });

     if(valid){
        Meteor.call('returnPurchase', purchase, returnedLines);
     }else{
        throwError(" ﻻ يمكن ارتجاع كمية اكبر من الكمية المسجلة في الفاتورة");
     }
  }
});

Template.returnPurchaseModal.helpers({
  lines : function(){
    var lines = Template.instance().data.fields.get('lines');
    _.each(lines, function(line, i){
      line.index = i;
    });
    return lines;
  },
  qty : function(line){
    var purchase = Template.instance().data.object;
    var returnedValues = purchase.returnValues();
    return line.qty - returnedValues[line.index];
  },
  subTotal : function(line){
    var purchase = Template.instance().data.object;
    var returnedValues = purchase.returnValues();
    return (line.qty - returnedValues[line.index]) * line.price;
  }
})