Package.describe({
  name: 'bpoint:supplier',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.1.0.2');

  api.use(['minimongo', 'mongo-livedata', 'mrt:moment', 'templating'], 'client');
  api.use('mongo');
  api.use('reywood:publish-composite', ['server']);
  api.use('emadshaaban:form-view');
  api.use('emadshaaban:list-view-wrapper');


  api.addFiles('supplier.js');

  api.addFiles('client/pay_first_term_value_modal.html', ['client']);

  api.addFiles('client/supplier.js', ['client']);
  api.addFiles('client/suppliers.js', ['client']);
  api.addFiles('client/supplier_payment.js', ['client']);
  api.addFiles('client/supplier_payments.js', ['client']);
  api.addFiles('client/pay_first_term_value_modal.js', ['client']);

  api.addFiles('server/publish.js', ['server']);

  api.export('Suppliers');
  api.export('SupplierPayments');


});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('supplier');
  api.addFiles('supplier-tests.js');
});
