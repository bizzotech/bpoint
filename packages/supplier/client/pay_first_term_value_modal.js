Template.payFirstTermValueModal.events({
  'click #payButton' : function(event, template){
     event.preventDefault();
     var paymentValue = Number($('#paymentValue').val());
     var supplier = template.data.object;
     Modal.hide('payFirstTermValueModal');
      
     var toPay = supplier.remainigOfFirstTermValue();
      if(paymentValue > toPay){
          throwError(" لا يمكن دفع مبلغ اكبر من المتبقي " + toPay);
          return 
      }
      Meteor.call('payFirstTermValue', supplier._id, paymentValue);
  }
});