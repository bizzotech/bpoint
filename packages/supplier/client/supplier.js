var supplierForm = {
    collection : Suppliers,
    name : "supplier",
    class : "",
    createTitle : " اضافة مورد " ,
    readTitle : " بيانات مورد ",
    updateTitle : " تعديل بيانات مورد " ,
    fieldGroups : [
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "الاسم",
                    name : "name",
                    type : "text",
                    required : true,
                },

                {
                    class : "col-xs-6",
                    label : "رقم التيليفون",
                    name : "phoneNumber",
                    type : "text",
                }
            ]
        },

        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "العنوان",
                    name : "address",
                    type : "text",
                },
                {
                    class : "col-xs-6",
                    label : "رصيد اول فترة",
                    name : "firstTermValue",
                    type : "number",
                    visible : function(collection, object, fields, formType){

                        return formType == "create" || !object.active ;
                    }
                }
            ]
        },
        
    ],
    submitButtonContent : "حفظ",
    editButtonContent : "تعديل",
    deleteButtonContent : "حذف",
    isDeletable : function(collection, object, fields){
        return false;
    },
    customButtons : [
        {
            name : 'payFirstTermValue',
            label : 'تسوية رصيد اول فترة',
            fn :function(collection, object, fields){
                if(object != undefined && object._id != undefined){
                    Modal.show('payFirstTermValueModal', {object : object, fields : fields});
                }
            },
            visible : function(collection, object, fields, formType){
                return formType == "read" && object.remainigOfFirstTermValue() > 0;
            }
        }

    ],
}

FormView.registerForm(Suppliers, supplierForm);