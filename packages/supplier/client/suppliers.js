var supplierList = {
  name : "supplier",
  title : "قائمة الموردين",
  fields : [
    {
      name : "name",
      label : "الاسم",
    },
    {
      name : "phoneNumber",
      label : "رقم التيليفون",
    },
    {
      name : "address",
      label : "العنوان",
    }
  ],
  tableClass : function(){
    return "table table-stripped table-hover";
  },
  fieldClass : function(object, fieldName){
    return fieldName;
  },
  virtualColumns : [
    {
      name : "toBePaid",
      label : "المتبقي له",
      fn : function(supplier){
        var val = supplier.toBePaid();
        return Number(Number(val).toFixed(2)) || 0;
      },
    },
    
  ]
}

ListView.registerList(Suppliers, supplierList);
