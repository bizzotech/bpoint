var supplierPaymentForm = {
    collection : SupplierPayments,
    name : "supplierpayment",
    class : "",
    createTitle : " تسجيل مقبوضات " ,
    readTitle : " بيانات مقبوضات ",
    updateTitle : " تعديل بيانات مقبوضات " ,
    fieldGroups : [
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "المورد",
                    name : "supplier",
                    type : "object",
                    collection : "suppliers",
                    formName : "supplier",
                    required : true,
                },

                {
                    label : "اسم العميل",
                    name : "supplierName",
                    type : "text",
                    fn : function(supplier){
                        if(supplier != undefined && supplier != ""){
                            return Suppliers.findOne(supplier).name; 
                        }
                        return "";
                    },
                    dependOn : ["supplier"],
                    style : "display : none",
                },

                {
                    class : "col-xs-6",
                    label : "التاريخ",
                    name : "date",
                    type : "date",
                    defaultValue : function(){
                        return new Date();
                    }
                }
            ]
        },

        {
            class : "",
            fields : [
                {
                    class : "col-xs-12",
                    label : "تم التأكيد",
                    name : "valid",
                    type : "state",
                    visible : function(){
                        return false;
                    },
                }
            ]
        },
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : " المبلغ ",
                    name : "value",
                    type : "number",
                    required : true,
                },
                {
                    class : "col-xs-6",
                    label : " المتبقي له ",
                    type : "number",
                    fn : function(supplier){
                        if(supplier != undefined && supplier != "" ){
                            return  Suppliers.findOne(supplier).toBePaid();
                        }
                        return 0;
                    },
                    dependOn : ['supplier'],
                    readOnly : true,
                    virtual : true,
                }
            ]
        }
    ],
    submitButtonContent : "حفظ",
    editButtonContent : "تعديل",
    deleteButtonContent : "حذف",
    isEditable : function(collection, object, fields){
        return object.valid != true && object.valid != "true";
    },
    isDeletable : function(collection, object, fields){
        return object.valid != true && object.valid != "true";
    },
    customButtons : [
        {
            name : "confirm",
            label : "تأكيد",
            fn : function(collection, object, fields){
                if(object != undefined && object._id != undefined){
                    Meteor.call('validateSupplierPayment', object);
                }
            },
            visible : function(collection, object, fields, formType){
                return formType == "read" && object.valid != true;
            }
        }
    ]
}

FormView.registerForm(SupplierPayments, supplierPaymentForm);