var supplierPaymentList = {
  name : "supplierpayment",
  title : "قائمة مدفوعات الموردين",
  readOnly : true,
  fields : [
    {
      name : "supplierName",
      label : "المورد",
    },
    {
      name : "value",
      label : "القيمة",
    }
  ],
  tableClass : function(){
    return "table table-stripped table-hover";
  },
  fieldClass : function(object, fieldName){
    return fieldName;
  },
  rowClass : function(item){
      if(!item.valid){
        return "info";
      }
      
      return "";
  },
  virtualColumns : [
    {
      name : "date",
      label : " التاريخ ",
      fn : function(supplierpayement){
         var date = supplierpayement.date;
         return moment(date).format('YYYY-MM-DD');
      }
    },
    
  ],
  sortKey : 'date'
}

ListView.registerList(SupplierPayments, supplierPaymentList);
