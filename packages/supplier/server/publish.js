SupplierPayments.before.insert(function(supplierpayment_id, doc){
  var supplier = Suppliers.findOne(doc.supplier);
  doc.supplierName = supplier.name;

  Suppliers.update(doc.supplier, {$set : {active : true} });
});

Suppliers.before.insert(function(supplier_id, doc){
  doc.active = false;
  doc.paidFromFirstTermValue = 0;
});