Suppliers = new Mongo.Collection("suppliers");

Suppliers.helpers({
  remainigOfFirstTermValue : function(){
    return this.firstTermValue - this.paidFromFirstTermValue;
  },
  total : function(){
    var t = this.firstTermValue || 0;
    var purchases = Purchases.find({supplier : this._id, valid : true}).fetch();
    _.each(purchases, function(purchase){
      t += purchase.total;
    });
    return t;
  },
  totalPayments : function(){
    var t = 0;
    var payments = SupplierPayments.find({supplier : this._id, valid : true}).fetch();
    _.each(payments, function(payment){
      t += payment.value;
    });
    return t;
  },
  toBePaid : function(){
    return this.total() - this.totalPayments();
  }
});

SupplierPayments = new Mongo.Collection("supplierpayments");

Meteor.startup(function () {

  Meteor.methods({
    validateSupplierPayment : function(supplierpayment){
      
      //Check that payment <= toBePaid for that supplier

      supplier = Suppliers.find({_id : supplierpayment.supplier}).fetch()[0];

      if(supplier.toBePaid() < supplierpayment.value){

        if(Meteor.isClient){

            throwError( "ﻻ يمكن ان يكون المبلغ المدفوع اكبر من المستحق للمورد" );

        }

        return;

      }
      
      SupplierPayments.update(supplierpayment._id, {$set : {valid : true}});
      
      
      var purchases = Purchases.find({supplier : supplierpayment.supplier, valid : true, paid : false}).fetch();


      var new_payment = supplierpayment.value;
      for(var i = 0 ; i < purchases.length ; i++){
        purchase = purchases[i];
        var toPay = purchase.total - purchase.totalPaid;
        if(new_payment < toPay){
          purchase.totalPaid += new_payment;
          Purchases.update(purchase._id, {$inc : {totalPaid : new_payment}}); 
          break;
        }else{
          Purchases.update(purchases[i]._id, {$inc : {totalPaid : toPay}, $set : {paid : true}});
          new_payment -= toPay;
        }
      }
      
    },
    payFirstTermValue : function(supplier_id, paymentValue){
      SupplierPayments.insert({ supplier : supplier_id, date : new Date(), valid : true, value : Number(paymentValue) });
    
      Suppliers.update(supplier_id, {$inc : {paidFromFirstTermValue : Number(paymentValue)} }); 
    }

  });
  
});