Template.payReturnSaleModal.events({
  'click #payButton' : function(event, template){
     event.preventDefault();
     var paymentValue = 0 - Number($('#paymentValue').val());
     var sale = template.data.object;
     Modal.hide('payReturnSaleModal');
      
     var toPay = sale.toPay();
      if(paymentValue < toPay){
          throwError(" لا يمكن رد مبلغ اكبر من اجمالي المردودات ");
          return 
      }
      Meteor.call('paySale', sale, toPay, paymentValue)
      
      template.data.fields.set('totalPaid', sale.totalPaid + Number(paymentValue));
      template.data.fields.set('toPay', sale.toPay() - Number(paymentValue));
      
  }
});