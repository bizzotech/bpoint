var saleReturnForm = {
    collection : SaleReturns,
    name : "salereturn",
    class : "",
    readTitle : " بيانات ارتجاع مبيعات ",
    fieldGroups : [
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "اسم العميل",
                    name : "customerName",
                    type : "text",
                },

                {
                    class : "col-xs-6",
                    label : "التاريخ",
                    name : "date",
                    type : "date",
                }
            ]
        },

        {
            class : "",
            fields : [
                {
                    class : "col-xs-12",
                    label : "تم التأكيد",
                    name : "valid",
                    type : "state",
                    visible : function(){
                        return false;
                    },
                }
            ]
        },
        
        {
            class : "",
            fields : [
                {
                    class : "col-xs-12",
                    label : "تفاصيل",
                    name : "lines",
                    type : "list",
                    fields : [
                        {
                            name : "productName",
                            label : "المنتج",
                            type : "text",
                        },
                        {
                            name : "qty",
                            label : "الكمية",
                            type : "number",
                        },
                        {
                            name : "price",
                            label : "السعر",
                            type : "number",
                        },
                        {
                            name : "subTotal",
                            label : "الاجمالي",
                            type : "number",
                        }
                    ]
                }
            ]
        },
        
        {
            class : "",
            fields : [
                {
                    class : "col-xs-6",
                    label : "الاجمالي",
                    name : "total",
                    type : "number",
                },

            ]
        },
    ],
    deleteButtonContent : "حذف",
    isSavable : function(){
        return false;
    },
    isEditable : function(collection, object, fields){
        return false;
    },
    isDeletable : function(collection, object, fields){
        return object.valid != true && object.valid != "true";
    },
    onDeleteClick : function(){
        
    },
    customButtons : [
        {
            name : "confirm",
            label : "تأكيد",
            fn : function(collection, object, fields){
                if(object != undefined && object._id != undefined){
                    Meteor.call('validateSaleReturn', object);
                }
            },
            visible : function(collection, object, fields, formType){
                return formType == "read" && object.valid != true;
            }
        }
    ]
}

FormView.registerForm(SaleReturns, saleReturnForm);

var saleForm = FormView.formStructures.sale;

saleForm.fieldGroups[5].fields.unshift({
    class : "col-xs-6",
    label : "المردود",
    name : "returnTotal",
    type : "number",
    fn : function(data){
        if(data.object)
            return data.object.returnTotal();
    },
    dependOn : "data",
    readOnly : true,
    virtual : true,
    visible : function(collection, object, fields, formType){
        return formType == "read";
    }
});

saleForm.customButtons.push({
        name : "return",
        label : "رد",
        fn : function(collection, object, fields){
            if(object != undefined && object._id != undefined){
                Modal.show('returnSaleModal', {object : object, fields : fields});
            }
        },
        visible : function(collection, object, fields, formType){
            return formType == "read" && object.valid == true && object.total > object.returnTotal();
        }
});
saleForm.customButtons.push({
        name : "viewReturns",
        label : "عرض المرتجعات",
        fn : function(collection, object, fields){
            if(object != undefined && object._id != undefined){
                Modal.show('returnsModal', {objects : object.returns(), formStructure : saleReturnForm, title : saleReturnForm.readTitle});
            }
        },
        visible : function(collection, object, fields, formType){
            return formType == "read" && object.valid == true && object.returns().count() > 0;
        }
});

saleForm.customButtons.push({
        name : "payReturns",
        label : "رد مبلغ",
        fn : function(collection, object, fields){
            if(object != undefined && object._id != undefined){
                Modal.show('payReturnSaleModal', {object : object, fields : fields});
            }
        },
        visible : function(collection, object, fields, formType){
            return formType == "read" && object.toPay() < 0 && object.valid == true;
        }

});

var saleList = ListView.listStructures.sale;

var parentRowClass = saleList.rowClass;

saleList.rowClass = function(item){
    if(item.toPay() < 0){
        return "danger";
    }
    return parentRowClass(item);
}