FormView.registerPublication(Sales, [
    {
        name : '_id',
        collection : SaleReturns,
        relatedName : 'sale',
    }
]);

FormView.registerPublication(CustomerPayments, [
    {
        name : 'customer',
        collection : SaleReturns,
        relatedName : 'customer',
    }
]);

FormView.registerPublication(SaleReturns);

ListView.registerPublication(Sales, [
    {
        name : '_id',
        collection : SaleReturns,
        relatedName : 'sale',
    }
]);

ListView.registerPublication(CustomerPayments, [
    {
        name : 'customer',
        collection : SaleReturns,
        relatedName : 'customer',
    }
]);



SaleReturns.before.insert(function(saleReturnId, doc){
    doc.valid = false;
  
});

ListView.registerPublication(Customers, [
    {
        name : '_id',
        collection : SaleReturns,
        relatedName : 'customer',
    }
]);

SelectElement.registerPublication(Customers, [
    {
        name : '_id',
        collection : SaleReturns,
        relatedName : 'customer',
    }
]);