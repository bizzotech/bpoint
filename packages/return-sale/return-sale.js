SaleReturns = new Mongo.Collection("salereturns");

SaleReturns.helpers({
  totalCost : function(){
    var total = 0;
    _.each(this.lines, function(line){
      total += line.cost;
    });
    return total;
  },
});

Sales.helpers({
    returns : function(){
        return SaleReturns.find({sale : this._id});
    },
    returnValues : function(){
        var returns = SaleReturns.find({sale : this._id});
        var returnValues = [];
        _.each(this.lines, function(line){
            returnValues.push(0);
        });
        if(returns.count() > 0){
            _.each(returns.fetch(), function(_return){
                _.each(_return.lines, function(line, i){
                    returnValues[i] += line.qty;
                });
            });
        }
        return returnValues;
    },
    returnTotal : function(){
        var returns = SaleReturns.find({sale : this._id, valid : true});
        var total = 0;
        _.each(returns.fetch(), function(_return){
            total += _return.total;
        });
        return total;
    },
    toPay : function(){
        return this.total - this.totalPaid - this.returnTotal();
    }
});

Customers.helpers({
    totalSaleReturns : function(){
        var returns = SaleReturns.find({customer : this._id, valid : true});
        var total = 0;
        _.each(returns.fetch(), function(_return){
            total += _return.total;
        });
        return total;
    },
    toBePaid : function(){
        return this.total() - this.totalPayments() - this.totalSaleReturns();
    }
})

Meteor.startup(function () {
    Meteor.methods({
        returnSale : function(sale, returnLines){
            //alert("Return" + sale.customerName);
            //console.log(returnLines);
            var total = 0;
            _.each(returnLines, function(line){
                total += line.subTotal;
            });
            SaleReturns.insert({
                sale : sale._id,
                customer : sale.customer,
                customerName : sale.customerName,
                date : new Date(),
                lines : returnLines,
                total : total,
            });
        },
        validateSaleReturn : function(saleReturn){
            SaleReturns.update(saleReturn._id, {$set : {valid : true} } );

            _.each(saleReturn.lines, function(line, i){
                if(line.qty > 0){
                  Products.update({_id : line.product}, {
                    $push : {
                      costTable : {
                        available : line.qty,
                        price : line.cost / line.qty, //line.price ,
                        saleReturn : saleReturn._id,
                        index : i,
                        sold : 0
                      }
                    }
                  });
                }


            });
        }
    })
});
